<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;
use DateTime;
use File;
use Session;
use App\MyFunc;
class LemburController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    


    /**
         public function __construct()
    {
        $this->middleware('auth');
    }    

     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $bln=date('m');
        $thn=date('Y');


        if($request->get('bln')){
            $bln=$request->get('bln');
        }
        if($request->get('thn')){
            $thn=$request->get('thn');
        }

        $url = env('API_BASE_URL')."/lembur?limit=100&bln=".$bln."&thn=".$thn;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }
        
        $param['bln']=$bln;
        $param['thn']=$thn;

        $param['data']=$data;
        return view('master.master')->nest('child', 'lembur.index',$param);
    }
    public function add_lembur(Request $request){
        $url = env('API_BASE_URL')."/absen/approval/list-usr-kontigensi";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data='';           
        }

        $param['data']=$data;
    
        return view('master.master')->nest('child', 'lembur.add_lembur',$param);
    }

     public function insert_lembur(Request $request){
        if((int) $request->input('jam_akhir') < (int) $request->input('jam_awal')){
            return json_encode(['rc'=>9999,'msg'=>'Jam akhir harus lebih besar dari jam mulai']);
        }

        $hari=date('D',strtotime($request->input('tgl')));
        $status=1;
        if($hari=='Mon'){
            $status=1;
        }else if($hari=='Tus'){
            $status=1;
        }else if($hari=='Wed'){
            $status=1;
        }else if($hari=='Thu'){
            $status=1;
        }else if($hari=='Fri'){
            $status=1;
        }else{
            $status=2;
        }

        if($status==1){

            if($request->input('jam_awal') > '17:30'){

            
                $url = env('API_BASE_URL')."/lembur/";
                $client = new Client();
                $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '. session('token')
                ];
                $data = array(
                    'tanggal'=> date('Y-m-d',strtotime($request->input('tgl'))),
                    'jam_mulai'=> $request->input('jam_awal'),
                    'jam_selesai'=> $request->input('jam_akhir'),
                    'perihal'=> $request->input('tujuan'),
                    'tempat'=> $request->input('tempat'),
                    'user_id_approval'=> (int) $request->input('user_approval')

                    
                );

                try{
                    
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);
                    
                    $param1=[];
                    $param1= (string) $result->getBody();
                    $data1 = json_decode($param1, true);
                    
                    //return $this->form_lembur($data1);
                      if($data1['statusCode']==200){
                    
                            session()->forget('id_lembur');
                            session()->forget('jam_mulai');
                            session()->forget('jam_selesai');

                            Session::put('id_lembur', $data1['data'][0]['id']);
                            Session::put('jam_mulai', $data1['data'][0]['jam_mulai']);
                            Session::put('jam_selesai', $data1['data'][0]['jam_selesai']);

                            return json_encode($data1);
                        }else{

                            return json_encode($data1);
                                
                        }

                    //return json_encode($data1);

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    //dd($response);
                    return json_encode($response);
                }

                }else{

                    return json_encode(['rc'=>9999,'msg'=>'Jam mulai lembur minimal harus 1 jam setelah jam keluar']);
                }



        }else{
            
            if( (int) $request->input('jam_awal') < 8){
                return json_encode(['rc'=>9999,'msg'=>'Ketentuan lembur untuk hari LIBUR dimulai jam 08:00 !']);
            }else{

                $url = env('API_BASE_URL')."/lembur/";
                $client = new Client();
                $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '. session('token')
                ];
                $data = array(
                    'tanggal'=> date('Y-m-d',strtotime($request->input('tgl'))),
                    'jam_mulai'=> $request->input('jam_awal'),
                    'jam_selesai'=> $request->input('jam_akhir'),
                    'perihal'=> $request->input('tujuan'),
                    'tempat'=> $request->input('tempat'),
                    'user_id_approval'=> (int) $request->input('user_approval')
                );

                try{
                    
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);
                    
                    $param1=[];
                    $param1= (string) $result->getBody();
                    $data1 = json_decode($param1, true);
                    
                    //return $this->form_lembur($data1);
                       if($data1['statusCode']==200){
                    
                            session()->forget('id_lembur');
                            session()->forget('jam_mulai');
                            session()->forget('jam_selesai');

                            Session::put('id_lembur', $data1['data'][0]['id']);
                            Session::put('jam_mulai', $data1['data'][0]['jam_mulai']);
                            Session::put('jam_selesai', $data1['data'][0]['jam_selesai']);

                            return json_encode($data1);
                        }else{

                            return json_encode($data1);
                                
                        }

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    //dd($response);
                    return json_encode($response);
                }

            
            }
                
        }

        


    }

    public function form_lembur(Request $request){
        
        $id=$request->get('id');
        $nomor=$request->get('nomor');
        $jam_mulai=$request->get('jam_mulai');
        $jam_selesai=$request->get('jam_selesai');
        $perihal=$request->get('perihal');
        $tanggal=$request->get('tanggal');
        $tempat=$request->get('tempat');

        $url = env('API_BASE_URL')."/lembur/anggota/".$id;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }
        
        $param['data']=$data;

        $param['id']=$id;
        $param['nomor']=$nomor;
        $param['jam_mulai']=$jam_mulai;
        $param['jam_selesai']=$jam_selesai;
        $param['perihal']=$perihal;
        $param['tanggal']=$tanggal;
        $param['tempat']=$tempat;

        return view('master.master')->nest('child', 'lembur.form',$param);


    }


    public function form_pengawas(Request $request){

        $id=$request->get('id');
        $nomor=$request->get('nomor');
        $jam_mulai=$request->get('jam_mulai');
        $jam_selesai=$request->get('jam_selesai');
        $perihal=$request->get('perihal');
        $tanggal=$request->get('tanggal');
        $tempat=$request->get('tempat');

        $url = env('API_BASE_URL')."/ref/unitkerja";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }
        
        $param['data']=$data;

        $param['id']=$id;
        $param['nomor']=$nomor;
        $param['jam_mulai']=$jam_mulai;
        $param['jam_selesai']=$jam_selesai;
        $param['perihal']=$perihal;
        $param['tanggal']=$tanggal;
        $param['tempat']=$tempat;

        return view('master.master')->nest('child', 'lembur.form_pengawas',$param);


    }

    public function form_petugas(Request $request){

        $id=$request->get('id');
        $nomor=$request->get('nomor');
        $jam_mulai=$request->get('jam_mulai');
        $jam_selesai=$request->get('jam_selesai');
        $perihal=$request->get('perihal');
        $tanggal=$request->get('tanggal');
        $tempat=$request->get('tempat');

        $url = env('API_BASE_URL')."/ref/unitkerja";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }
        
        $param['data']=$data;

        $param['id']=$id;
        $param['nomor']=$nomor;
        $param['jam_mulai']=$jam_mulai;
        $param['jam_selesai']=$jam_selesai;
        $param['perihal']=$perihal;
        $param['tanggal']=$tanggal;
        $param['tempat']=$tempat;

        return view('master.master')->nest('child', 'lembur.form_petugas',$param);


    }

    public function pegawai($id){

        $url = env('API_BASE_URL')."/ref/pegawai-unit/".$id;
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];

        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            
            //return $this->form_lembur($data1);
            return json_encode($data1['data']);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //dd($response);
            return $response;
        }
    }

    public function insert_pengawas(Request $request){

        if($request->input('jam_akhir') < $request->input('jam_awal')){
            return json_encode(['rc'=>9999,'msg'=>'Jam akhir harus lebih besar dari jam mulai']);
        }

        $hari=date('D',strtotime($request->input('tanggal')));
        $status=1;
        if($hari=='Mon'){
            $status=1;
        }else if($hari=='Tus'){
            $status=1;
        }else if($hari=='Wed'){
            $status=1;
        }else if($hari=='Thu'){
            $status=1;
        }else if($hari=='Fri'){
            $status=1;
        }else{
            $status=2;
        }


        if($status==1){
            if($request->input('jam_awal') > '17:30'){

                $id=$request->input('id');
                $nomor=$request->input('nomor');
                $tanggal=$request->input('tanggal');
                $perihal=$request->input('perihal');
                $tempat=$request->input('tempat');
                $jam_mulai=$request->input('jam_mulai');
                $jam_selesai=$request->input('jam_selesai');

                $url = env('API_BASE_URL')."/lembur/pengawas";
                $client = new Client();
                $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '. session('token')
                ];
                $data = array(
                    "lembur_id"=> (int) Session('id_lembur'),
                    "nrp"=> $request->input('id_pengawas'),
                    "jam_mulai"=> $request->input('jam_awal'),
                    "jam_selesai"=> $request->input('jam_akhir')
                );

                try{
                    
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);
                    
                    $param1=[];
                    $param1= (string) $result->getBody();
                    $data1 = json_decode($param1, true);
                    
                    return json_encode($data1);
                    //return $this->form_lembur($data1);
                    //return json_encode(['id'=>$id,'nomor'=>$nomor,'tanggal'=>$tanggal,'perihal'=>$perihal,'tempat'=>$tempat,'jam_mulai'=>$jam_mulai,'jam_selesai'=>$jam_selesai]);

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    //dd($response);
                    return json_encode($response);
                }

                }else{

                    return json_encode(['rc'=>9999,'msg'=>'Jam mulai lembur minimal harus 1 jam setelah jam keluar']);
                }

        }else{

            $id=$request->input('id');
            $nomor=$request->input('nomor');
            $tanggal=$request->input('tanggal');
            $perihal=$request->input('perihal');
            $tempat=$request->input('tempat');
            $jam_mulai=$request->input('jam_mulai');
            $jam_selesai=$request->input('jam_selesai');

            $url = env('API_BASE_URL')."/lembur/pengawas";
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "lembur_id"=> (int) Session('id_lembur'),
                "nrp"=> $request->input('id_pengawas'),
                "jam_mulai"=> $request->input('jam_awal'),
                "jam_selesai"=> $request->input('jam_akhir')
            );

            try{
                
                $result = $client->post($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                
                return json_encode($data1);
                //return $this->form_lembur($data1);
                //return json_encode(['id'=>$id,'nomor'=>$nomor,'tanggal'=>$tanggal,'perihal'=>$perihal,'tempat'=>$tempat,'jam_mulai'=>$jam_mulai,'jam_selesai'=>$jam_selesai]);

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                //dd($response);
                return json_encode($response);
            }
        }
        


        
    }

    public function insert_petugas(Request $request){


        if($request->input('jam_akhir') < $request->input('jam_awal')){
            return json_encode(['rc'=>9999,'message'=>'Jam akhir harus lebih besar dari jam mulai']);
        }

        $hari=date('D',strtotime($request->input('tanggal')));
        $status=1;
        if($hari=='Mon'){
            $status=1;
        }else if($hari=='Tus'){
            $status=1;
        }else if($hari=='Wed'){
            $status=1;
        }else if($hari=='Thu'){
            $status=1;
        }else if($hari=='Fri'){
            $status=1;
        }else{
            $status=2;
        }

        if($status==1){
            if($request->input('jam_awal') > '17:30'){
                
                $id=$request->input('id');
                $nomor=$request->input('nomor');
                $tanggal=$request->input('tanggal');
                $perihal=$request->input('perihal');
                $tempat=$request->input('tempat');
                $jam_mulai=$request->input('jam_mulai');
                $jam_selesai=$request->input('jam_selesai');

                $url = env('API_BASE_URL')."/lembur/petugas";
                $client = new Client();
                $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '. session('token')
                ];
                $data = array(
                    "lembur_id"=> (int) Session('id_lembur'),
                    "nrp"=> $request->input('id_petugas'),
                    "jam_mulai"=> $request->input('jam_awal'),
                    "jam_selesai"=> $request->input('jam_akhir')
                );


                try{
                    
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);
                    
                    $param1=[];
                    $param1= (string) $result->getBody();
                    $data1 = json_decode($param1, true);
                    
                    //return $this->form_lembur($data1);
                    return json_encode($data1);
                    //return json_encode(['id'=>$id,'nomor'=>$nomor,'tanggal'=>$tanggal,'perihal'=>$perihal,'tempat'=>$tempat,'jam_mulai'=>$jam_mulai,'jam_selesai'=>$jam_selesai]);

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    //dd($response);
                    return json_encode($response);
                }

            }else{

                return json_encode(['rc'=>9999,'message'=>'Jam mulai lembur minimal harus 1 jam setelah jam keluar']);
            }   
        }else{

             $id=$request->input('id');
            $nomor=$request->input('nomor');
            $tanggal=$request->input('tanggal');
            $perihal=$request->input('perihal');
            $tempat=$request->input('tempat');
            $jam_mulai=$request->input('jam_mulai');
            $jam_selesai=$request->input('jam_selesai');

            $url = env('API_BASE_URL')."/lembur/petugas";
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "lembur_id"=> (int) Session('id_lembur'),
                "nrp"=> $request->input('id_petugas'),
                "jam_mulai"=> $request->input('jam_awal'),
                "jam_selesai"=> $request->input('jam_akhir')
            );

            try{
                
                $result = $client->post($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                
                return json_encode($data1);
                //return $this->form_lembur($data1);
               //return json_encode(['id'=>$id,'nomor'=>$nomor,'tanggal'=>$tanggal,'perihal'=>$perihal,'tempat'=>$tempat,'jam_mulai'=>$jam_mulai,'jam_selesai'=>$jam_selesai]);

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                //dd($response);
                return json_encode($response);
            }
        }     

       

    }

     public function kwintansilembur(Request $request){
        
       $param['id']=$request->get('id');
        return view('master.master')->nest('child', 'lembur.upload',$param);

    }

    public function insert_kwitansi_lembur(Request $request){


        // $media = $request->file('file');
        if($request->hasFile('file')){
            $file = $request->file('file');

            // Mendapatkan Nama File
            $nama_file = $file->getClientOriginalName();
       
            // Mendapatkan Extension File
            $extension = $file->getClientOriginalExtension();

            $file_mime = $file->getmimeType();
            $fileContent = File::get($file);
        }

        $url = env('API_BASE_URL')."/lembur/upload/".$request->input('id');
        $client = new Client();

       try {
            $result = $client->put(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => $nama_file,
                            'Mime-Type'=> $file_mime,
                            'extension'=> $extension
                        ]
                        
                        
                    ],
                ]
            );

             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);

             //dd($data_result);
            return json_encode($data_result);
             
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //$bad_response = $this->responseData($e->getCode(), $response);
            //dd($response);
            return json_encode($response);
        }
    }

    public function insert_rincian_hasil(Request $request){
         $url = env('API_BASE_URL')."/lembur/hasil-kerja/".Session('id_lembur');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "rincian_kerja"=> $request->input('rincian'),
                "hasil_kerja"=> $request->input('hasil')
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                
                //return $this->form_lembur($data1);
                return json_encode($data1);

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                //dd($response);
                return json_encode($response);
            }
    }

    public function next_lembur(Request $request){

        if($request->get('id')){
            $id=$request->get('id');
        }else{
            $id=Session('id_lembur');    
        }
        
        $url = env('API_BASE_URL')."/lembur/".$id;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data='';           
        }

        $url2 = env('API_BASE_URL')."/lembur/anggota/".$id;
        $client2 = new Client();
        $headers2 = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result2 = $client2->get($url2,[
                RequestOptions::HEADERS => $headers2,
                'verify'=>false
                ]);
            
            
            $param2=[];
            $param2= (string) $result2->getBody();
            $data2 = json_decode($param2, true);
            $data2 =$data2['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data2='';           
        }
        $param['data_lembur']=$data;
        $param['data_peserta']=$data2;
        $param['id']=$id;
    
        return view('master.master')->nest('child', 'lembur.add_lembur_next',$param);


    }
    public function hapus_peserta(Request $request){
        
       // $param['id']=$request->get('id');
        //return view('master.master')->nest('child', 'perdin.upload',$param);

        $url = env('API_BASE_URL')."/lembur/peserta/".$request->get('id');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->delete($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
        }
    }
    public function cetak_lembur(Request $request){
       $fields=[];
       //$failed = \DB::select("select * from lembur where id=".$request->get('id'));
       
       $failed = \DB::select("select l.*,pg.nmpegawai,j.nama,l.jam_selesai::time - l.jam_mulai::time as durasi from lembur l
            left join pegawai pg on pg.id=l.user_id_approval
            left join jabatan j on j.id=pg.idjab
            where l.id=".$request->get('id'));
       

        if($request->get('jns')==1){
            $keterangan='Berita Acara';
            return MyFunc::pdf($failed,$keterangan,$fields,'lembur.berita_acara');
        }else{
            $keterangan='Surat Tugas Lembur';
            return MyFunc::pdf($failed,$keterangan,$fields,'lembur.surat_perintah');
        }
       


        DB::table('lembur')
        ->where('id', $request->get('id'))
        ->update(['is_print' => true]);
       
    }

}
