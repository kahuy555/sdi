<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;
use DateTime;
use File;
use Session;
use Redirect;
use App\MyFunc;
use PDF;
class PerdinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    


    /**
         public function __construct()
    {
        $this->middleware('auth');
    }    

     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $bln=date('m');
        $thn=date('Y');

         if($request->get('bln')){
            $bln=$request->get('bln');
        }
        if($request->get('thn')){
            $thn=$request->get('thn');
        }
       
        $url = env('API_BASE_URL')."/perdin?limit=100&bln=".$bln."&thn=".$thn;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }


        $param['data']=$data;
        $param['bln']=$bln;
        $param['thn']=$thn;
        return view('master.master')->nest('child', 'perdin.index',$param);
    }

    public function add_perdin(Request $request){
        $url = env('API_BASE_URL')."/absen/approval/list-usr-kontigensi";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data='';           
        }

        $param['data']=$data;
    
        return view('master.master')->nest('child', 'perdin.add_perdin',$param);
    }

     public function insert_perdin(Request $request){


        /*
        session()->forget('start');
        session()->forget('end');
        session()->forget('uraian');
        session()->forget('jns');
        session()->forget('user_approval');
        session()->forget('pemberi_tugas');
        session()->forget('wilayah');
        session()->forget('nama_wilayah');
        */


        Session::put('start', date('Y-m-d',strtotime($request->input('start'))));
        Session::put('end', date('Y-m-d',strtotime($request->input('end'))));
        Session::put('uraian', $request->input('uraian'));
        Session::put('jns', $request->input('jns'));
        Session::put('user_approval', $request->input('user_approval'));
        Session::put('pemberi_tugas', $request->input('pemberi_tugas'));
        Session::put('wilayah', $request->input('wilayah'));
        Session::put('nama_wilayah', $request->input('nama_wilayah'));

        return json_encode(['rcode'=>200,'msg'=>'next step']);

        /*
        $url = env('API_BASE_URL')."/perdin";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        $data = array(
            "tgl_pengajuan"=> date('Y-m-d'),
            "tgl_mulai"=> date('Y-m-d',strtotime($request->input('start'))),
            "tgl_selesai"=> date('Y-m-d',strtotime($request->input('end'))),
            "jns_perdin"=> $request->input('jns'),
            "user_id_svp"=> (int) $request->input('user_approval'),
            "wilayah_id"=> (int) $request->input('wilayah'),
            "tujuan"=> $request->input('destination'),
            "asal"=> $request->input('origin'),
            "catatan"=> "",
            "uraian"=> $request->input('uraian'),
            "user_id_pemberi"=> (int) $request->input('user_approval'),
            "province_id"=> (int) $request->input('province')
        );

        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            return json_encode($data1['data']);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return $response;
        }

        */

    }

    public function add_next(Request $request){

        $url = env('API_BASE_URL')."/perdin/wilayah?wilayah=".Session('wilayah');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];

        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            
            //return $this->form_lembur($data1);
            //return json_encode($data1['data']['data']);
            $data=$data1['data']['data'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //dd($response);
            //return $response;
            $data='';

        }

        $param['provinsi']=$data;
     
        return view('master.master')->nest('child', 'perdin.add_next',$param);
    }

    public function wilayah($id){

        $url = env('API_BASE_URL')."/perdin/wilayah?wilayah=".$id;
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];

        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            
            //return $this->form_lembur($data1);
            return json_encode($data1['data']['data']);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //dd($response);
            return $response;
        }
    }

     public function form_pegawai(Request $request){


        $id=$request->get('id');
        $nomor=$request->get('nomor');
        $tgl_mulai=$request->get('tgl_mulai');
        $tgl_selesai=$request->get('tgl_selesai');
        $jns_perdin=$request->get('jns_perdin');
        $destination=$request->get('destination');
        $origin=$request->get('origin');

        $url = env('API_BASE_URL')."/ref/unitkerja";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }
        
        $param['data']=$data;

        $param['id']=$id;
        $param['nomor']=$nomor;
        $param['tgl_mulai']=$tgl_mulai;
        $param['tgl_selesai']=$tgl_selesai;
        $param['jns_perdin']=$jns_perdin;
        $param['destination']=$destination;
        $param['origin']=$origin;

        return view('master.master')->nest('child', 'perdin.form_pegawai',$param);


    }

      public function form_perdin(Request $request){


        $id=$request->get('id');
        $nomor=$request->get('nomor');
        $tgl_mulai=$request->get('tgl_mulai');
        $tgl_selesai=$request->get('tgl_selesai');
        $jns_perdin=$request->get('jns_perdin');
        $user=$request->get('user');
        $wilayah=$request->get('wilayah');
        $destination=$request->get('destination');
        $origin=$request->get('origin');

        $url = env('API_BASE_URL')."/perdin/peserta/".$id;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }


        $url1 = env('API_BASE_URL')."/perdin/".$id;
        $client1 = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result1 = $client1->get($url1,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data2 = json_decode($param2, true);
            $data1 =$data2['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }
        $param['data']=$data;
        $param['data1']=$data1;

        $param['id']=$id;
        $param['nomor']=$nomor;
        $param['tgl_mulai']=$tgl_mulai;
        $param['tgl_selesai']=$tgl_selesai;
        $param['jns_perdin']=$jns_perdin;
        $param['user']=$user;
        $param['wilayah']=$wilayah;
        $param['destination']=$destination;
        $param['origin']=$origin;

        return view('master.master')->nest('child', 'perdin.form',$param);


    }

    public function insert_pegawai(Request $request){

        /*
        $id=$request->input('id');
        $nomor=$request->input('nomor');
        $tgl_mulai=$request->input('tgl_mulai');
        $tgl_selesai=$request->input('tgl_selesai');
        $jns_perdin=$request->input('jns_perdin');
        $destination=$request->input('destination');
        $origin=$request->input('origin');

        */

        $url = env('API_BASE_URL')."/perdin/peserta";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        $data = array(
            "perdin_id"=> (int) Session('id_perdin'),
            "nrp"=> $request->input('id_pegawai'),
        );

        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            
            //return $this->form_lembur($data1);
            //return json_encode(['id'=>$id,'nomor'=>$nomor,'tgl_mulai'=>$tgl_mulai,'tgl_selesai'=>$tgl_selesai,'jns_perdin'=>$jns_perdin,'destination'=>$destination,'origin'=>$origin]);
            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //dd($response);
            return json_encode($response);
        }
    }

    public function hapus_pegawai(Request $request){
        
       // $param['id']=$request->get('id');
        //return view('master.master')->nest('child', 'perdin.upload',$param);

        $url = env('API_BASE_URL')."/perdin/peserta/".$request->get('id');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->delete($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
        }
    }

     public function hapus_perdin(Request $request){
        
       // $param['id']=$request->get('id');
        //return view('master.master')->nest('child', 'perdin.upload',$param);

        $url = env('API_BASE_URL')."/perdin/".$request->get('id');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->delete($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            return $data1;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return $response;
        }
    }

     public function kwintansi(Request $request){
        
       $param['id']=$request->get('id');
        return view('master.master')->nest('child', 'perdin.upload',$param);

    }

    public function insert_kwitansi(Request $request){


        // $media = $request->file('file');
        if($request->hasFile('file')){
            $file = $request->file('file');

            // Mendapatkan Nama File
            $nama_file = $file->getClientOriginalName();
       
            // Mendapatkan Extension File
            $extension = $file->getClientOriginalExtension();

            $file_mime = $file->getmimeType();
            $fileContent = File::get($file);
        }

        $url = env('API_BASE_URL')."/perdin/".$request->input('id');
        $client = new Client();

       try {
            $result = $client->put(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => $nama_file,
                            'Mime-Type'=> $file_mime,
                            'extension'=> $extension
                        ]
                        
                        
                    ],
                ]
            );

             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);

             //dd($data_result);
            return json_encode($data_result);
             
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //$bad_response = $this->responseData($e->getCode(), $response);
            //dd($response);
            return json_encode($response);
        }
    }

     public function insert_perdin_new(Request $request){

        /*
        request
        array:11 [
          "_token" => "ZJPO9ADUe7rGhE1WSJYqRtgMYK2oSzbplGnRCFRp"
          "start" => "2020-02-11"
          "end" => "2020-02-11"
          "uraian" => "ddd"
          "jns" => "Dinas"
          "wilayah" => "1"
          "user_approval" => "6"
          "province" => "1"
          "origin" => "cc"
          "tujuan" => "1"
          "destination" => "cc"
        ]
        */

        $url = env('API_BASE_URL')."/perdin";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        $data = array(
            "tgl_pengajuan"=> date('Y-m-d'),
            "tgl_mulai"=> $request->input('start'),
            "tgl_selesai"=> $request->input('end'),
            "jns_perdin"=> $request->input('jns'),
            "user_id_svp"=> (int) $request->input('user_approval'),
            "wilayah_id"=> (int) $request->input('wilayah'),
            "tujuan"=> $request->input('destination'),
            "asal"=> $request->input('origin'),
            "catatan"=> "",
            "uraian"=> $request->input('uraian'),
            "user_id_pemberi"=> (int) $request->input('user_approval'),
            "province_id"=> (int) $request->input('province')
        );

        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);

            

            if($data1['statusCode']==200){
                    
                session()->forget('id_perdin');
                Session::put('id_perdin', $data1['data'][0]['id']);

                return json_encode($data1);
            }else{

                return json_encode($data1);
                    
            }

            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
        }
    }

    public function detail_perdin(Request $request){
        if($request->get('id')){
            $id=$request->get('id');
        }else{
            $id=Session('id_perdin');
        }
        

        $url1 = env('API_BASE_URL')."/perdin/".$id;
        $client1 = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result1 = $client1->get($url1,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data2 = json_decode($param2, true);
            $data1 =$data2['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data1='';
        }

        $url = env('API_BASE_URL')."/perdin/peserta/".$id;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data='';
        }


        $param['data_perdin']=$data1;
        $param['data_peserta']=$data;
        $param['id']=$id;
        return view('master.master')->nest('child', 'perdin.detail_perdin',$param);
    }

    public function cetak_perdin(Request $request){
       $fields=[];
       $failed = \DB::select("select p.id,p.nomor,pg.nmpegawai,j.nama,p.asal,p.tujuan,p.wilayah_id,w.nama as wilayah,p.uraian,p.durasi,p.tgl_mulai,p.tgl_selesai from perdin p
            left join pegawai pg on pg.id=p.user_id_svp
            left join jabatan j on j.id=pg.idjab
            left join wilayah_perdin w on w.id=p.wilayah_id
            where p.id=".$request->get('id'));


       if($request->get('jns')==1){

        $keterangan='Surat Perintah Perjalanan Dinas';
        return MyFunc::pdf($failed,$keterangan,$fields,'perdin.surat_perintah');

       }else{

        $keterangan='Perincian Ongkos Perjalanan Dinas';
        return MyFunc::pdf($failed,$keterangan,$fields,'perdin.surat_perincian');
        
       }

        DB::table('perdin')
        ->where('id', $request->get('id'))
        ->update(['is_print' => true]);
       
    }
}
