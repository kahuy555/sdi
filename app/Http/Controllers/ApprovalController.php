<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Redirect;

class ApprovalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    


    /**
         public function __construct()
    {
        $this->middleware('auth');
    }    

     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $pilih=$request->get('pilih');

        if($pilih==2){

           $pilih=$request->get('pilih');
            $url = env('API_BASE_URL')."/cuti/approval/list-cuti?limit=1000";
            $client = new Client();
            $headers = [
                'Authorization' => 'Bearer '. session('token')
            ];
            try{
                
                $result = $client->get($url,[
                    RequestOptions::HEADERS => $headers,
                    'verify'=>false
                    ]);
                
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                $data =$data1;
            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                $data='';
            }
            
        }else if($pilih==3){
            $pilih=$request->get('pilih');
            $url = env('API_BASE_URL')."/lembur/approval/list-lembur?limit=1000";
            $client = new Client();
            $headers = [
                'Authorization' => 'Bearer '. session('token')
            ];
            try{
                
                $result = $client->get($url,[
                    RequestOptions::HEADERS => $headers,
                    'verify'=>false
                    ]);
                
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                $data =$data1;
            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                $data='';
            }



        }else if($pilih==4){
            $pilih=$request->get('pilih');
            $url = env('API_BASE_URL')."/perdin/approval/list-perdin?limit=1000";
            $client = new Client();
            $headers = [
                'Authorization' => 'Bearer '. session('token')
            ];
            try{
                
                $result = $client->get($url,[
                    RequestOptions::HEADERS => $headers,
                    'verify'=>false
                    ]);
                
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                $data =$data1;
            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                $data='';
            }


        }else{

            $pilih='1';
            $url = env('API_BASE_URL')."/absen/approval/list-kontigensi?limit=1000";
            $client = new Client();
            $headers = [
                'Authorization' => 'Bearer '. session('token')
            ];
            try{
                
                $result = $client->get($url,[
                    RequestOptions::HEADERS => $headers,
                    'verify'=>false
                    ]);
                
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                $data =$data1;
            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                $data='';
            }
        }

        $param['pilih']=$pilih;
        $param['data']=$data;

        return view('master.master')->nest('child', 'approval.index',$param);
    }

    public function approve(Request $request){


        if($request->get('type')=='kontigensi'){

            $url = env('API_BASE_URL')."/absen/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di approve",
                "status"=> 1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }


        }elseif($request->get('type')=='cuti'){

            $url = env('API_BASE_URL')."/cuti/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di approve",
                "status"=> 1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }


        }elseif($request->get('type')=='lembur'){
            $url = env('API_BASE_URL')."/lembur/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di approve",
                "status"=> 1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }

        }else{

            $url = env('API_BASE_URL')."/perdin/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di approve",
                "status"=> 1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }



        }

        

    }


    public function reject(Request $request){


        if($request->get('type')=='kontigensi'){

            $url = env('API_BASE_URL')."/absen/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di reject",
                "status"=> -1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }


        }elseif($request->get('type')=='cuti'){

            $url = env('API_BASE_URL')."/cuti/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di reject",
                "status"=> -1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }


        }elseif($request->get('type')=='lembur'){
            $url = env('API_BASE_URL')."/lembur/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di reject",
                "status"=> -1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }

        }else{

            $url = env('API_BASE_URL')."/perdin/approval/".$request->get('id');
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];
            $data = array(
                "catatan"=> "Sudah di reject",
                "status"=> -1
            );

            try{
                
                $result = $client->put($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data,
                ]);
                
                $param1=[];
                $param1= (string) $result->getBody();
                $data1 = json_decode($param1, true);
                return $data1;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                return $response;
            }



        }

        

    }


}

