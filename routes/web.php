<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});


*/
Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::get( '/', ['as'=>'home', 'uses'=> 'HomeController@index']);
Route::post('login1',['as'=>'login1', 'uses'=> 'AuthController@LoginViaAPI']);
Route::get('/libur','HomeController@libur');
Route::get('/list_pegawai','HomeController@list_pegawai');



//absen
Route::get('absensi', 'AbsensiController@index')->name('absensi');
Route::get('add_kontigensi', 'AbsensiController@add_kontigensi');
Route::post('insert_kontigensi',['as'=>'insert_kontigensi', 'uses'=> 'AbsensiController@insert_kontigensi']);


//cuti
Route::get('cuti', 'CutiController@index')->name('cuti');
Route::get('add_cuti', 'CutiController@add_cuti')->name('add_cuti');
Route::post('insert_cuti',['as'=>'insert_cuti', 'uses'=> 'CutiController@insert_cuti']);
Route::get('hapus_cuti', 'CutiController@hapus_cuti');

//lembur
Route::get('lembur', 'LemburController@index')->name('lembur');
Route::get('add_lembur', 'LemburController@add_lembur')->name('add_lembur');
Route::post('insert_lembur',['as'=>'insert_lembur', 'uses'=> 'LemburController@insert_lembur']);
Route::post('insert_pengawas',['as'=>'insert_pengawas', 'uses'=> 'LemburController@insert_pengawas']);
Route::post('insert_petugas',['as'=>'insert_petugas', 'uses'=> 'LemburController@insert_petugas']);
Route::get('kwintansilembur', 'LemburController@kwintansilembur');
Route::post('insert_kwitansi_lembur',['as'=>'insert_kwitansi_lembur', 'uses'=> 'LemburController@insert_kwitansi_lembur']);
Route::post('insert_rincian_hasil',['as'=>'insert_rincian_hasil', 'uses'=> 'LemburController@insert_rincian_hasil']);
Route::get('next_lembur',['as'=>'next_lembur', 'uses'=> 'LemburController@next_lembur']);
//Route::post('insert_lembur', 'LemburController@insert_lembur');
Route::get('/list_lembur','LemburController@list_lembur');
Route::get('/form_lembur','LemburController@form_lembur');
Route::get('/form_pengawas','LemburController@form_pengawas');
Route::get('/form_petugas','LemburController@form_petugas');
Route::get('/pegawai/{id}','LemburController@pegawai');
Route::get('hapus_peserta', 'LemburController@hapus_peserta');
Route::get('cetak_lembur', 'LemburController@cetak_lembur');


//perdin
Route::get('perdin', 'PerdinController@index')->name('perdin');
Route::get('add_perdin', 'PerdinController@add_perdin')->name('add_perdin');
Route::get('add_next', 'PerdinController@add_next')->name('add_next');
Route::post('insert_perdin',['as'=>'insert_perdin', 'uses'=> 'PerdinController@insert_perdin']);
Route::post('insert_perdin_new',['as'=>'insert_perdin_new', 'uses'=> 'PerdinController@insert_perdin_new']);
Route::post('insert_pegawai',['as'=>'insert_pegawai', 'uses'=> 'PerdinController@insert_pegawai']);
Route::get('/form_perdin','PerdinController@form_perdin');
Route::get('/form_pegawai','PerdinController@form_pegawai');
Route::get('/wilayah/{id}','PerdinController@wilayah');
Route::get('hapus_perdin', 'PerdinController@hapus_perdin');
Route::get('kwintansi', 'PerdinController@kwintansi');
Route::post('insert_kwitansi',['as'=>'insert_kwitansi', 'uses'=> 'PerdinController@insert_kwitansi']);
Route::get('detail_perdin',['as'=>'detail_perdin', 'uses'=> 'PerdinController@detail_perdin']);
Route::get('hapus_pegawai', 'PerdinController@hapus_pegawai');
Route::get('cetak_perdin', 'PerdinController@cetak_perdin');



//ukes
Route::get('ukes', 'UkesController@index')->name('ukes');
Route::get('ukesk', 'UkesController@ukesk')->name('ukesk');
Route::get('/form_ukes','UkesController@form_ukes');
Route::post('insert_ukes',['as'=>'insert_ukes', 'uses'=> 'UkesController@insert_ukes']);
Route::get('/gettanggungan','UkesController@gettanggungan');


//approval
Route::get('approval', 'ApprovalController@index')->name('approval');


Route::get('approve', 'ApprovalController@approve');
Route::get('reject', 'ApprovalController@reject');



