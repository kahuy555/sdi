<!DOCTYPE html>
<html lang="en">
@include('master.head')
	<body style="background-image: url({{asset('assets/media/demos/demo4/header.jpg')}}); background-position: center top; background-size: 100% 350px;" class="kt-page--loading-enabled kt-page--loading kt-page--fixed kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">
		<div id="loading">
            <div class="lds-facebook">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="demo4/index.html">
				<img alt="Logo" src="{{asset('assets/media/logos/logo-4-sm.png')}}" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
		</div>
	</div>
			<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
						<div class="kt-container">

							<!-- begin:: Brand -->
							<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
								<a class="kt-header__brand-logo" href="demo4/index.html">
									<img alt="Logo" src="{{asset('assets/media/logos/logo-4.png')}}" class="kt-header__brand-logo-default" />
									<img alt="Logo" src="{{asset('assets/media/logos/logo-4-sm.png')}}" class="kt-header__brand-logo-sticky" />
								</a>
							</div>

							<!-- end:: Brand -->

							<!-- begin: Header Menu -->
							<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
							@include('master.menu')

							<!-- end: Header Menu -->

							<!-- begin:: Header Topbar -->
							<div class="kt-header__topbar kt-grid__item">
							@php
							$longname = Session('name');
							$split = explode(' ', $longname);
							$name = $split[0];
							@endphp	
								<div class="kt-header__topbar-item kt-header__topbar-item--user">
									<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
										<span class="kt-header__topbar-welcome">Hi,</span>
										<span class="kt-header__topbar-username">{{$name}}</span>
										<span class="kt-header__topbar-icon"><b>S</b></span>
										<img alt="Pic" src="./assets/media/users/300_21.jpg" class="kt-hidden" />
									</div>
									<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

										<!--begin: Head -->
										<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
											<div class="kt-user-card__avatar">
											<!--	
												<img class="kt-hidden" alt="Pic" src="{{asset('assets/media/users/300_25.jpg')}}" />

											-->
												<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
												<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">S</span>
											</div>
											<div class="kt-user-card__name">
												{{Session('name')}}
											</div>
										</div>

										<!--end: Head -->

										<!--begin: Navigation -->
										<div class="kt-notification">
											<!--
											<a href="!#" class="kt-notification__item">
												<div class="kt-notification__item-icon">
													<i class="flaticon2-calendar-3 kt-font-success"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														My Profile
													</div>
													<div class="kt-notification__item-time">
														Account settings and more
													</div>
												</div>
											</a>
										-->
											<div class="kt-notification__custom kt-space-between">
												<a href="demo4/custom/user/login-v2.html" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold" onclick="event.preventDefault();document.getElementById('logout-form').submit();" href="{{ route('logout') }}">Sign Out</a>
												 
						                           <form id="logout-form" action="{{ route('logout') }}" method="POST"
						                                  style="display: none;">
						                                  {{ csrf_field() }}
						                            </form>
											</div>
										</div>

										<!--end: Navigation -->
									</div>
								</div>

								<!--end: User bar -->
							</div>

							<!-- end:: Header Topbar -->
						</div>
					</div>

					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								@php
								$kalimat='Dashboard';
								if(Request::is('absensi')){
									$kalimat='Absensi';
								}
								if(Request::is('cuti')){
									$kalimat='Cuti';
								}
								if(Request::is('lembur')){
									$kalimat='Lembur';
								}
								if(Request::is('perdin')){
									$kalimat='Perdin';
								}
								if(Request::is('ukes')){
									$kalimat='Uang Kesehatan';
								}
								if(Request::is('approal')){
									$kalimat='Approval';
								}
								@endphp
								<!-- begin:: Subheader -->
							<!--	
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">{{$kalimat}}</h3>
										<div class="kt-subheader__breadcrumbs">
											<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
											<span class="kt-subheader__breadcrumbs-separator"></span>
											<a href="" class="kt-subheader__breadcrumbs-link">
												{{$kalimat}} </a>
										</div>
									</div>
								</div>

							-->


								<!-- end:: Subheader -->

								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid">
									<br>
									<!--Begin::Dashboard 4-->
									@yield('content')
									
								</div>

								<!-- end:: Content -->
							</div>
						</div>
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer" style="background-image: url('{{asset('assets/media/bg/bg-2.jpg')}}');">
						<div class="kt-footer__top">
							<div class="kt-container">
								<div class="row">
									<div class="col-lg-4">
										<div class="kt-footer__section">
											<div class="kt-footer__content">
											<div id="canvas-holder" style="width:100%">
												<canvas id="chart-area"></canvas>
											</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="kt-footer__section">
											<h3 class="kt-footer__title">Other Links</h3>
											<div class="kt-footer__content">
												<div class="kt-footer__nav">
													<div class="kt-footer__nav-section">
														<a href="#">Bank Bjb Syariah</a>
														<a href="#">Portal Kepatuhan</a>
														<a href="#">Portal Budaya Perusahaan</a>
													</div>
													<div class="kt-footer__nav-section">
														<a href="#">Email Internal</a>
														<a href="#">Olibs Syariah 724</a>
														<a href="#">Brosur Digital</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="kt-footer__section">
											<h3 class="kt-footer__title">Approval Request</h3>
											<div class="kt-footer__content">
												<div class="kt-footer__nav">
													<div class="kt-footer__nav-section">
														<p>Cuti <span style="margin-right:30%" class="float-right">:</span></p>
														<p>Lembur <span style="margin-right:30%" class="float-right">:</span></p>
														<p>Perdin <span style="margin-right:30%" class="float-right">:</span></p>
														<p>Absensi <span style="margin-right:30%" class="float-right">:</span></p>
													</div>
													<div class="kt-footer__nav-section">
														<p>
															<a href="/cuti" style="margin-left:50%">
																<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">
																10</span>
															</a>
														</p>
														<p>
															<a href="/cuti" style="margin-left:50%">
																<span class="kt-badge kt-badge--warning kt-badge--inline text-white kt-badge--pill">
																21</span>
															</a>
														</p>
														<p>
															<a href="/cuti" style="margin-left:50%">
																<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill">
																5</span>
															</a>
														</p>
														<p>
															<a href="/cuti" style="margin-left:50%">
																<span class="kt-badge kt-badge--primary kt-badge--inline kt-badge--pill">
																7</span>
															</a>
														</p>
													</div>
													<div class="kt-footer__nav-section">
														<p></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-footer__bottom">
							<div class="kt-container">
								<div class="kt-footer__wrapper">
									<div class="kt-footer__logo">
										<a class="kt-header__brand-logo" href="demo4/index&amp;demo=demo2.html">
											<img alt="Logo" src="{{asset('assets/media/logos/logo-4-sm.png')}}" class="kt-header__brand-logo-sticky">
										</a>
										<div class="kt-footer__copyright">
											2019&nbsp;&copy;&nbsp;
											<a href="{{URL::to('/')}}">HRMIS</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

@include('master.script')
@yield('script')
<div id="kt_scrolltop" class="kt-scrolltop">
	<i class="fa fa-arrow-up"></i>
</div>
<script>
	window.onload = function() {
		var ctx = document.getElementById('chart-area').getContext('2d');
		window.myDoughnut = new Chart(ctx, config);
	};
</script>
</body>		
</html>