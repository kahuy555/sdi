@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__head kt-portlet__head--lg">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon">
								<i class="kt-font-brand flaticon2-line-chart"></i>
							</span>
							<h3 class="kt-portlet__head-title">
								Approval
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-wrapper">
								<select class="form-control" id="pilih" name="pilih">
									<option value="1" @if($pilih==1) selected @endif>Kontigensi</option>
									<option value="2" @if($pilih==2) selected @endif>Cuti</option>
									<option value="3" @if($pilih==3) selected @endif>Lembur</option>
									<option value="4" @if($pilih==4) selected @endif>Perdin</option>
								</select>
							</div>
						</div>
					</div>

					<div class="kt-portlet__body kt-portlet__body--fit">

						
						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<!--begin:: Widgets/Applications/User/Profile3-->
							@if($pilih==1)
							@if($data['data']['data'])
								<table class="table table-striped- table-hover" id="table_id" width="100%">
								@foreach($data['data']['data'] as $item)
								<tr>
								<div class="kt-portlet">
									<div class="kt-portlet__body">
										<div class="kt-widget kt-widget--user-profile-3">
											<div class="kt-widget__top">
												<div class="kt-widget__media kt-hidden">
													<img src="./assets/media/users/100_1.jpg" alt="image">
												</div>
												<div class="kt-widget__pic kt-widget__pic--brand kt-font-brand kt-font-boldest kt-hidden-">
													AA
												</div>
												<div class="kt-widget__content">
													<div class="kt-widget__head">
														<a href="#" class="kt-widget__username">
															{{$item['nama']}}
														</a>
														<div class="kt-widget__action">
															<button type="button" onclick="approve('{{$item['id']}}','kontigensi')" class="btn btn-label-success btn-sm btn-upper">Approve</button>&nbsp;
															<button type="button" onclick="reject('{{$item['id']}}','kontigensi')" class="btn btn-label-danger btn-sm btn-upper">Reject</button>
														</div>
													</div>
													<div class="kt-widget__subhead">
														<a href="#"><i class="flaticon2-calendar-3"></i>{{$item['nrp']}}</a>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['alasan']}}
														</div>
													</div>
												</div>
											</div>
											<div class="kt-widget__bottom">
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-event-calendar-symbol"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Pengajuan</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['jam_masuk']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Jam Datang</span>
														<span class="kt-widget__value"><span>{{date('H:i',strtotime($item['jam_masuk']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Jam Pulang</span>
														<span class="kt-widget__value"><span>{{date('H:i',strtotime($item['jam_keluar']))}}</span>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>	
								</tr>
								@endforeach
								</table>	
								
							@endif

							@elseif($pilih==2)

							@if($data['data']['data'])
							<table class="table table-striped- table-hover" id="table_id" width="100%">
								@foreach($data['data']['data'] as $item)
								<tr>
								<div class="kt-portlet">
									<div class="kt-portlet__body">
										<div class="kt-widget kt-widget--user-profile-3">
											<div class="kt-widget__top">
												<div class="kt-widget__media kt-hidden">
													<img src="./assets/media/users/100_1.jpg" alt="image">
												</div>
												<div class="kt-widget__pic kt-widget__pic--brand kt-font-brand kt-font-boldest kt-hidden-">
													AA
												</div>
												<div class="kt-widget__content">
													<div class="kt-widget__head">
														<a href="#" class="kt-widget__username">
															{{$item['nama']}}
														</a>
														<div class="kt-widget__action">
															<button type="button" onclick="approve('{{$item['id']}}','cuti')" class="btn btn-label-success btn-sm btn-upper">Approve</button>&nbsp;
															<button type="button" onclick="reject('{{$item['id']}}','cuti')" class="btn btn-label-danger btn-sm btn-upper">Reject</button>
														</div>
													</div>
													<div class="kt-widget__subhead">
														<a href="#"><i class="flaticon2-calendar-3"></i>{{$item['nrp']}}</a>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['alasan']}}
														</div>
													</div>
												</div>
											</div>
											<div class="kt-widget__bottom">
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-event-calendar-symbol"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Pengajuan</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['tgl_pengajuan']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Mulai</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['tgl_mulai']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Selesai</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['tgl_selesai']))}}</span>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>	
								</tr>
								@endforeach

							</table>
								
								
							@endif

							@elseif($pilih==4)

							@if($data['data']['data'])
							<table class="table table-striped- table-hover" id="table_id" width="100%">
								@foreach($data['data']['data'] as $item)
								<tr>
								<div class="kt-portlet">
									<div class="kt-portlet__body">
										<div class="kt-widget kt-widget--user-profile-3">
											<div class="kt-widget__top">
												<div class="kt-widget__media kt-hidden">
													<img src="./assets/media/users/100_1.jpg" alt="image">
												</div>
												<div class="kt-widget__pic kt-widget__pic--brand kt-font-brand kt-font-boldest kt-hidden-">
													AA
												</div>
												<div class="kt-widget__content">
													<div class="kt-widget__head">
														<a href="#" class="kt-widget__username">
															{{$item['nama']}}
														</a>
														<div class="kt-widget__action">
															<button type="button" onclick="approve('{{$item['id']}}','perdin')" class="btn btn-label-success btn-sm btn-upper">Approve</button>&nbsp;
															<button type="button" onclick="reject('{{$item['id']}}','perdin')" class="btn btn-label-danger btn-sm btn-upper">Reject</button>
														</div>
													</div>
													<div class="kt-widget__subhead">
														<a href="#"><i class="flaticon2-calendar-3"></i>{{$item['nrp']}}</a>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['jns_perdin']}}
														</div>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['wilayah']}}
														</div>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['asal']}}
														</div>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['tujuan']}}
														</div>
													</div>
												</div>
											</div>
											<div class="kt-widget__bottom">
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-event-calendar-symbol"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Pengajuan</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['tgl_pengajuan']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Mulai</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['tgl_mulai']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Selesai</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['tgl_selesai']))}}</span>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>	
								</tr>
								@endforeach

							</table>
								
							@endif


							@else
							@if($data['data']['data'])
							<table class="table table-striped- table-hover" id="table_id" width="100%">
								@foreach($data['data']['data'] as $item)
								<tr>
									<td>
								<div class="kt-portlet">
									<div class="kt-portlet__body">
										<div class="kt-widget kt-widget--user-profile-3">
											<div class="kt-widget__top">
												<div class="kt-widget__media kt-hidden">
													<img src="./assets/media/users/100_1.jpg" alt="image">
												</div>
												<div class="kt-widget__pic kt-widget__pic--brand kt-font-brand kt-font-boldest kt-hidden-">
													AA
												</div>
												<div class="kt-widget__content">
													<div class="kt-widget__head">
														<a href="#" class="kt-widget__username">
															{{$item['nama']}}
														</a>
														<div class="kt-widget__action">
															<button type="button" onclick="approve('{{$item['id']}}','lembur')" class="btn btn-label-success btn-sm btn-upper">Approve</button>&nbsp;
															<button type="button" onclick="reject('{{$item['id']}}','lembur')" class="btn btn-label-danger btn-sm btn-upper">Reject</button>
														</div>
													</div>
													<div class="kt-widget__subhead">
														<a href="#"><i class="flaticon2-calendar-3"></i>{{$item['nrp']}}</a>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['perihal']}}
														</div>
													</div>
													<div class="kt-widget__info">
														<div class="kt-widget__desc">
														{{$item['tempat']}}
														</div>
													</div>
												</div>
											</div>
											<div class="kt-widget__bottom">
												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-event-calendar-symbol"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Tanggal Pengajuan</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['tanggal']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Jam Mulai</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['jam_mulai']))}}</span>
													</div>
												</div>

												<div class="kt-widget__item">
													<div class="kt-widget__icon">
														<i class="flaticon-calendar-with-a-clock-time-tools"></i>
													</div>
													<div class="kt-widget__details">
														<span class="kt-widget__title">Jam Selesai</span>
														<span class="kt-widget__value"><span>{{date('d M Y',strtotime($item['jam_selesai']))}}</span>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>	
								</td>
								</tr>
								@endforeach
							</table>	
								
							@endif	



							@endif
							
							<!--end:: Widgets/Applications/User/Profile3-->
							{{--
							<!--Begin::Section-->
							<div class="row">
								<div class="col-xl-12">

									<!--begin:: Components/Pagination/Default-->
									<div class="kt-portlet">
										<div class="kt-portlet__body">

											<!--begin: Pagination-->
											<div class="kt-pagination kt-pagination--brand">
												<ul class="kt-pagination__links">
													<li class="kt-pagination__link--first">
														<a href="#"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
													</li>
													<li class="kt-pagination__link--next">
														<a href="#"><i class="fa fa-angle-left kt-font-brand"></i></a>
													</li>
													<li>
														<a href="#">...</a>
													</li>
													<li>
														<a href="#">29</a>
													</li>
													<li>
														<a href="#">30</a>
													</li>
													<li class="kt-pagination__link--active">
														<a href="#">31</a>
													</li>
													<li>
														<a href="#">32</a>
													</li>
													<li>
														<a href="#">33</a>
													</li>
													<li>
														<a href="#">34</a>
													</li>
													<li>
														<a href="#">...</a>
													</li>
													<li class="kt-pagination__link--prev">
														<a href="#"><i class="fa fa-angle-right kt-font-brand"></i></a>
													</li>
													<li class="kt-pagination__link--last">
														<a href="#"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
													</li>
												</ul>
												<div class="kt-pagination__toolbar">
													<select class="form-control kt-font-brand" style="width: 60px">
														<option value="10">10</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="50">50</option>
														<option value="100">100</option>
													</select>
													<span class="pagination__desc">
														Displaying 10 of 230 records
													</span>
												</div>
											</div>

											<!--end: Pagination-->
										</div>
									</div>

									<!--end:: Components/Pagination/Default-->
								</div>
							</div>
							--}}

							<!--End::Section-->
						</div>
						<!--end: Datatable -->
					</div>
				</div>
		</div>
	</div>
</div>
@include('absensi.action')
@endsection
@section('script')
<script type="text/javascript">
	$('#pilih').on('change', function (v) {
    	
    	$.ajax({
            type: 'GET',
            url: base_url + '/approval?pilih='+$('#pilih').val(),
			beforeSend: function () {
               $("#alertInfo").addClass('d-none');
               $("#alertError").addClass('d-none');
               $("#alertSuccess").addClass('d-none');
               $("#loading").css('display', 'block');
            },

            success: function (response) {
               console.log(response);
               //var response=JSON.parse(response);
               $("#loading").css('display', 'none');             
                window.location.href = base_url + '/approval?pilih='+$('#pilih').val();

                }

           }).done(function (msg) {
                $("#loading").css('display', 'none');
            }).fail(function (msg) {
                $("#loading").css('display', 'none');
                        // toastr.error("Terjadi Kesalahan");
            });


    });
    function approve(id,type){
    	 swal.fire({
            title: "Info",
           text: "Approve Data",
           type: "info",
           showCancelButton: true,
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {

            	 $.ajax({
                    type: 'GET',
                    url: base_url + '/approve?id='+id+'&type='+type,
                    beforeSend: function () {
						$("#alertInfo").addClass('d-none');
						$("#alertError").addClass('d-none');
						$("#alertSuccess").addClass('d-none');
						$("#loading").css('display', 'block');
					},

                    success: function (res) {
						$("#loading").css('display', 'none');
                        swal.fire({
                            title: "Informasi",
                            text: "Berhasil",
                            type: "success",
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                     $("#loading").css('display', 'none');

                }).fail(function(res) {
                   $("#loading").css('display', 'none');
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });

                
            }
        });
    }
    function reject(id,type){
    	 swal.fire({
            title: "Info",
           text: "Reject Data",
           type: "info",
           showCancelButton: true,
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {

            	 $.ajax({
                    type: 'GET',
                    url: base_url + '/reject?id='+id+'&type='+type,
                    beforeSend: function () {
						$("#alertInfo").addClass('d-none');
						$("#alertError").addClass('d-none');
						$("#alertSuccess").addClass('d-none');
						$("#loading").css('display', 'block');
					},

                    success: function (res) {
						$("#loading").css('display', 'none');
                        swal.fire({
                            title: "Informasi",
                            text: "Berhasil",
                            type: "success",
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                     $("#loading").css('display', 'none');

                }).fail(function(res) {
                   $("#loading").css('display', 'none');
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
                
            }
        });
    }
</script>
@stop