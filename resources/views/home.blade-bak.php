@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">

		<!--begin:: Widgets/Quick Stats-->
		<div class="row row-full-height">
			<div class="col-sm-12 col-md-12 col-lg-6">
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-brand">
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget26">
							<div class="kt-widget26__content">
								<span class="kt-widget26__number">570</span>
								<span class="kt-widget26__desc">ADKOM</span>
							</div>
							<div class="kt-widget26__chart" style="height:100px; width: 230px;">
								<canvas id="kt_chart_quick_stats_1"></canvas>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-space-20"></div>
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-danger">
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget26">
							<div class="kt-widget26__content">
								<span class="kt-widget26__number">640</span>
								<span class="kt-widget26__desc">RKP</span>
							</div>
							<div class="kt-widget26__chart" style="height:100px; width: 230px;">
								<canvas id="kt_chart_quick_stats_2"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-6">
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-success">
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget26">
							<div class="kt-widget26__content">
								<span class="kt-widget26__number">234+</span>
								<span class="kt-widget26__desc">BUDAYA</span>
							</div>
							<div class="kt-widget26__chart" style="height:100px; width: 230px;">
								<canvas id="kt_chart_quick_stats_3"></canvas>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-space-20"></div>
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-warning">
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget26">
							<div class="kt-widget26__content">
								<span class="kt-widget26__number">4.4M$</span>
								<span class="kt-widget26__desc">DIKLAT</span>
							</div>
							<div class="kt-widget26__chart" style="height:100px; width: 230px;">
								<canvas id="kt_chart_quick_stats_4"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--end:: Widgets/Quick Stats-->
	</div>

</div>
@endsection