@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Pengajuan Klaim Kesehatan
								</h3>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div class="kt-portlet__body">
								<div class="form-group">
									<label for="exampleSelect1">NIP</label>
									<input type="text" name="nip" class="form-control"  readonly value="{{session('nip')}}">
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Jenis Kesehatan</label>
									<select class="form-control"  name="jns_kesehatan" id="jns_kesehatan">
										<option value="">Silahkan Pilih</option>
										@foreach($data['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nama']}}</option>
										@endforeach
									</select>
									<div class="invalid-feedback">Silahkan pilih</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Tanggal Periksa</label>
									<div class="input-group date">
										<input type="text" class="form-control" name="tgl" readonly="" placeholder="Select date" id="kt_datepicker_2">
											<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-calendar-check-o"></i>
											</span>
										</div>

									</div>		
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Jumlah Biaya</label>
									<div class="input-group date">
										<div class="input-group-append">
											<span class="input-group-text">
												Rp
											</span>
										</div>
										<input type="text" onkeyup="convertToRupiah(this)"  class="form-control" name="nominal" id="nominal">
										
									</div>		
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Nama RS / Dokter</label>
									<input type="text" name="nama_rs" class="form-control">
									<div class="invalid-feedback">Silahkan isi tujuan</div>
								</div>

								<div class="form-group">
													<label>Untuk</label>
													<div class="kt-radio-inline">
														<label class="kt-radio kt-radio--solid kt-radio--brand">
															<input type="radio" name="tanggungan" id="cekpegawai" checked value="0"> Pegawai
															<span></span>
														</label>
														<label class="kt-radio kt-radio--solid kt-radio--brand">
															<input type="radio" name="tanggungan" id="cekpasangan" value="1"> Lainnya
															<span></span>
														</label>
													</div>
												<div></div></div>
								<div class="form-group">
									<label for="exampleSelect1">Struk</label>
									<input type="file" name="file" class="form-control">
								</div>
								<div class="form-group" id="pasien" style="display: none;">
									<label for="exampleSelect1">Nama Pasien</label>
									<select class="form-control"  name="nama_pasien" id="nama_pasien">
										
									</select>
								</div>	
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Keterangan</label>
									<textarea class="form-control" id="keterangan" name="keterangan" rows="2"></textarea>
									<div class="invalid-feedback">Silahkan isi keterangan</div>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button onclick="insert()" class="btn btn-primary">Submit</button>
									<button onclick="loadNewPage('{{ route('ukes') }}')" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('ukes.action')
@endsection
@section('script')
<script type="text/javascript">
	
	$(document).ready(function() {
		$('#form_kontigensi input').on('change', function() {

		   if($('input[name=tanggungan]:checked', '#form_kontigensi').val()==0){

		   		$('#pasien').hide();

		   }else{
		   		var _items='';
				$.ajax({
			        type: 'GET',
			        url: base_url + '/gettanggungan',
			        success: function (res) {
			            var data = $.parseJSON(res);
			            _items='<option value="">Silahkan pilih</option>';
			            $.each(data, function (k,v) {
			                _items += "<option value='"+v.id+"'>"+v.nama+"</option>";
			            });

			            $('#nama_pasien').html(_items);
			        }
			    });
		   		$('#pasien').show();
		   }

		});


	});		
	
		$('#wilayah').on('change', function (v) {

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/wilayah/'+this.value,
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.id+"'>"+v.nama+"</option>";
	            });

	            $('#province').html(_items);
	        }
	    });


	});
</script>
@stop