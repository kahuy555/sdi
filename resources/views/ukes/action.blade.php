<script type="text/javascript">
	

	function insert() {

		if($('#jns_kesehatan').val()===''){
			//endLoadingPage();
			$( "#jns_kesehatan" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jns_kesehatan").removeClass( "is-invalid" );

		}

		if($('#tgl').val()===''){
			//endLoadingPage();
			$( "#tgl" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tgl").removeClass( "is-invalid" );

		}

		if($('#nama_rs').val()===''){
			//endLoadingPage();
			$( "#nama_rs" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#nama_rs").removeClass( "is-invalid" );

		}

		if($('#keterangan').val()===''){
			//endLoadingPage();
			$( "#keterangan" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#keterangan").removeClass( "is-invalid" );

		}

		 var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_ukes') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);
				console.log(response['nomor']);

				$("#loading").css('display', 'none');


				
				if(response['statusCode']==200){
					 swal.fire({
			           title: "Info",
			           text: "Klaim ukes sudah disimpan ",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {
			                window.location.href = base_url + '/ukes';
			            }
			        });

					
				}else{
					var str = response['message'];
					var res = str.substring(40);
					$('#nominal').val(res);
					swal.fire("error",response['message'],"error");	  

				}
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

</script>