@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<!--Begin::App-->
							<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app" id="kt_app">
								<button class="kt-app__aside-close" id="kt_app_aside_close"><i class="la la-close"></i></button>
								<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_app_aside">

									<!--begin:: Widgets/Applications/User/Profile1-->
									<div class="kt-portlet kt-portlet--height-fluid-">
										<div class="kt-portlet__head  kt-portlet__head--noborder">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
												</h3>
											</div>
											<div class="kt-portlet__head-toolbar">
												<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
													<i class="flaticon-more-1"></i>
												</a>
												<div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md">
													<!--combo tahun-->
												</div>
											</div>
										</div>
										<div class="kt-portlet__body kt-portlet__body--fit-y">

											<!--begin::Widget -->
											<div class="kt-widget kt-widget--user-profile-1">
												<div class="kt-widget__head">
													<div class="kt-widget__media">
														<img src="{{asset('assets/media/users/300_25.jpg')}}" alt="image">
													</div>
													<div class="kt-widget__content">
														<div class="kt-widget__section">
															<a href="#" class="kt-widget__username">
																{{Session('name')}}
															</a>
															<span class="kt-widget__subtitle">
																{{Session('nrp')}}
															</span>
														</div>
													</div>
												</div>
												<div class="kt-widget__body">
													<div class="kt-widget__content">
														<div class="kt-widget__info">
															<span class="kt-widget__label">Jenis Kelamin</span>
															<span class="kt-widget__data">
																Laki-laki
															</span>
														</div>
														<div class="kt-widget__info">
															<span class="kt-widget__label">Tanggal Lahir</span>
															<span class="kt-widget__data">
																10 Oktober 2019
															</span>
														</div>
														<div class="kt-widget__info">
															<span class="kt-widget__label">Unit Kerja</span>
															<span class="kt-widget__data">
																DIVISI TEKNOLOGI INFORMASI
															</span>
														</div>
														<div class="kt-widget__info">
															<span class="kt-widget__label">Jabatan</span>
															<span class="kt-widget__data">
																Manager Pengembangan TI
															</span>
														</div>
													</div>
													<div class="kt-widget__items">
														<a onclick="tanggungan()" id="tanggungan1" class="kt-widget__item kt-widget__item--active">
															<span class="kt-widget__section">
																<span class="kt-widget__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<polygon id="Bound" points="0 0 24 0 24 24 0 24" />
																			<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero" />
																			<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3" />
																		</g>
																	</svg> </span>
																<span class="kt-widget__desc">
																	Data Tanggungan
																</span>
															</span>
														</a>
														<a onclick="pegawai()" id="pegawai1" class="kt-widget__item ">
															<span class="kt-widget__section">
																<span class="kt-widget__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
																			<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" id="Mask" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																			<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" id="Mask-Copy" fill="#000000" fill-rule="nonzero" />
																		</g>
																	</svg> </span>
																<span class="kt-widget__desc">
																	Pegawai
																</span>
															</span>
														</a>
														<a onclick="riwayat()" id="riwayat1" class="kt-widget__item ">
															<span class="kt-widget__section">
																<span class="kt-widget__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect id="bound" x="0" y="0" width="24" height="24" />
																			<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" id="Combined-Shape" fill="#000000" opacity="0.3" />
																			<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" id="Combined-Shape" fill="#000000" />
																		</g>
																	</svg> </span>
																<span class="kt-widget__desc">
																	Riwayat Cover Ukes
																</span>
																</spandiv>
														</a>
													</div>
												</div>
											</div>

											<!--end::Widget -->
										</div>
									</div>

									<!--end:: Widgets/Applications/User/Profile1-->
								</div>
								<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
									
									<div class="kt-portlet" id="tanggungan">
										<div class="row">
											<div class="col-xl-12">
												<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
														<div class="kt-portlet kt-portlet--mobile">
															<div class="kt-portlet__head kt-portlet__head--lg">
																<div class="kt-portlet__head-label">
																	<span class="kt-portlet__head-icon">
																		<i class="kt-font-brand flaticon2-line-chart"></i>
																	</span>
																	<h3 class="kt-portlet__head-title">
																		Data Tanggungan
																	</h3>
																</div>
															</div>
															<div class="kt-portlet__body kt-portlet__body--fit">
																<table class="table table-striped- table-hover"  width="100%">
																	<thead>
																		<tr>
																			<th title="Field #1">Keterangan</th>
																			<th title="Field #2">Nama Tanggungan</th>
																			<th title="Field #3">Tanggal Lahir</th>
																			<th title="Field #4">Jenis Kelamin</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td>Istri</td>
																			<td>SSBBYY</td>
																			<td>10 Okt 2009</td>
																			<td>P</td>
																		</tr>
																		<tr>
																			<td>Anak 1</td>
																			<td>XSBY1</td>
																			<td>10 Okt 2019</td>
																			<td>P</td>
																		</tr>
																		<tr>
																			<td>Anak 2</td>
																			<td>XSBY2</td>
																			<td>10 Okt 2019</td>
																			<td>L</td>
																		</tr>
																		<tr>
																			<td>Anak 3</td>
																			<td>XSBY3</td>
																			<td>10 Okt 2019</td>
																			<td>L</td>
																		</tr>
																	</tbody>
																</table>

																<!--end: Datatable -->
															</div>
														</div>
												</div>
											</div>
										</div>
									</div>	

									<!--pegawai-->

									<div class="kt-portlet" id="pegawai">
										<div class="row">
											<div class="col-xl-12">
												<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
														<div class="kt-portlet kt-portlet--mobile">
															<div class="kt-portlet__head kt-portlet__head--lg">
																<div class="kt-portlet__head-label">
																	<span class="kt-portlet__head-icon">
																		<i class="kt-font-brand flaticon2-line-chart"></i>
																	</span>
																	<h3 class="kt-portlet__head-title">
																		Pegawai
																	</h3>
																</div>
															</div>
															<div class="kt-portlet__body kt-portlet__body--fit">
																<table class="table table-striped-table-hover" width="100%">
																	<thead>
																		<tr>
																			<th title="Field #1">Nama</th>
																			<th title="Field #2">Plafon</th>
																			<th title="Field #3">Sisa Plafon</th>
																		</tr>
																	</thead>
																	<tbody>
																		@if($data_tanggungan)
																		@foreach($data_tanggungan['data'] as $item)
																		<tr>
																			<td>{{$item['nama']}}</td>
																			<td>{{number_format($item['plafon'],0,',','.')}}</td>
																			<td>{{number_format($item['sisa_plafon'],0,',','.')}}</td>
																		</tr>
																		@endforeach
																		@endif
																	</tbody>
																</table>

																<!--end: Datatable -->
															</div>
														</div>
												</div>
											</div>
										</div>
									</div>	



									<!--cover -->
									<div class="kt-portlet" id="riwayat">
										<div class="row">
											<div class="col-xl-12">
												<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
														<div class="kt-portlet kt-portlet--mobile">
															<div class="kt-portlet__head kt-portlet__head--lg">
																<div class="kt-portlet__head-label">
																	<span class="kt-portlet__head-icon">
																		<i class="kt-font-brand flaticon2-line-chart"></i>
																	</span>
																	<h3 class="kt-portlet__head-title">
																		Riwayat Cover Uang Kesehatan
																	</h3>
																</div>
																<div class="kt-portlet__head-toolbar">
																	<div class="kt-portlet__head-wrapper">
																		<button type="button" onclick="tambah_ukes();" class="btn btn-label-info btn-sm btn-upper">Pengajuan Klaim</button>
																	</div>
																</div>
															</div>
															<div class="kt-portlet__body kt-portlet__body--fit">
																<table class="table table-striped-table-hover" id="table_id" width="100%">
																	<thead>
																		<tr>
																			<th title="Field #1">No</th>
																			<th title="Field #2">Tanggal</th>
																			<th title="Field #3">Jumlah</th>
																			<th title="Field #4">Uraian</th>
																			<th title="Field #4">Untuk</th>
																			<th title="Field #4">Nama Tanggungan</th>
																			<th title="Field #4">Nama RS / Dokter</th>
																		</tr>
																	</thead>
																	<tbody>
																		@if($data_history['data'])
																		@php
																		$no=0;
																		@endphp
																		@foreach($data_history['data'] as $item)
																		@php
																		$no++;
																		@endphp
																		<tr>
																			<td>{{$no}}</td>
																			<td>{{date('d M Y',strtotime($item['tgl_periksa']))}}</td>
																			<td>{{number_format($item['nominal'],0,',','.')}}</td>
																			<td>-</td>
																			<td>-</td>
																			<td>{{$item['nama_tanggungan']}}</td>
																			<td>{{$item['nama_rs_dr']}}</td>
																		</tr>
																		@endforeach
																		@endif
																	</tbody>
																</table>

																<!--end: Datatable -->
															</div>
														</div>
												</div>
											</div>
										</div>
									</div>	
									
								</div>
							</div>

							<!--End::App-->
						</div>


@stop
@section('script')
<script type="text/javascript">
	$('#tanggungan').show();
	$('#pegawai').hide();
	$('#riwayat').hide();
    function tanggungan() {
    $('#tanggungan').show();
	$('#pegawai').hide();
	$('#riwayat').hide();
	
	$( "#tanggungan1" ).addClass("kt-widget__item--active");
	$( "#pegawai1" ).removeClass( "kt-widget__item--active" );
	$( "#riwayat1" ).removeClass( "kt-widget__item--active" );

    }
    function pegawai() {
    $('#pegawai').show();
    $('#riwayat').hide();
    $('#tanggungan').hide();

    $( "#pegawai1" ).addClass("kt-widget__item--active");
	$( "#tanggungan1" ).removeClass( "kt-widget__item--active" );
	$( "#riwayat1" ).removeClass( "kt-widget__item--active" );

    }

    function riwayat() {
    $('#riwayat').show();
    $('#pegawai').hide();
    $('#tanggungan').hide();

    $( "#riwayat1" ).addClass("kt-widget__item--active");
	$( "#tanggungan1" ).removeClass( "kt-widget__item--active" );
	$( "#pegawai1" ).removeClass( "kt-widget__item--active" );

    }


	function tambah_ukes(){
		$.ajax({
			type: 'GET',
			url: base_url + '/form_ukes',
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/form_ukes';
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

</script>
@stop