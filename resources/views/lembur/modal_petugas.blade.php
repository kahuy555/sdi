<div class="modal fade" id="kt_modal_petugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Petugas</h5>
			</div>
			<div class="modal-body">
				<form id="form_petugas">
					<input type="hidden" name="jam_awal" value="{{session('jam_mulai')}}">
					<input type="hidden" name="jam_akhir" value="{{session('jam_selesai')}}">
					<div>
						<label for="example-email-input" class="col-4 col-form-label">Nama Pegawai</label>
						<div class="col-8">
							<select name="id_petugas" id="id_petugas" class="form-control select">
							</select>
							<div class="invalid-feedback">Silahkan Pilih Pegawai</div>
						</div>
					</div>
					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="batal_add_petugas()">Batal</button>
				<button type="button" class="btn btn-primary" onclick="insert_petugas()">Simpan</button>
			</div>
		</div>
	</div>
</div>