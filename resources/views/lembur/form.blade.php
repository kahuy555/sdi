@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Lembur
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-wrapper">
								<button type="button" onclick="pengawas('{{$id}}','{{$nomor}}','{{$tanggal}}','{{$jam_mulai}}','{{$jam_selesai}}','{{$perihal}}','{{$tempat}}');" class="btn btn-label-info btn-sm btn-upper">Tambah Pengawas</button>
								&nbsp;
								<button type="button" onclick="petugas('{{$id}}','{{$nomor}}','{{$tanggal}}','{{$jam_mulai}}','{{$jam_selesai}}','{{$perihal}}','{{$tempat}}');" class="btn btn-label-info btn-sm btn-upper">Tambah Petugas</button>
							</div>
						</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi9">
							{{ csrf_field() }}
							<div class="kt-portlet__body">
								<div class="form-group">
									<label for="exampleSelect1">Nomor</label>
									<div class="input-group date">
										<input type="text" class="form-control" name="tgl" readonly value="{{$nomor}}">
											
									</div>		
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Tanggal</label>
									<div class="input-group date">
										<input type="text" class="form-control" name="tgl" readonly placeholder="Select date" value="{{date('d M Y',strtotime($tanggal))}}">
											<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-calendar-check-o"></i>
											</span>
										</div>
									</div>		
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Perkiraan Jam</label>
									<div class="row">
									<div class="col-6">
										<input class="form-control" name="jam_awal" readonly value="{{$jam_mulai}}" type="text">	
									</div>
									<div class="col-6">
										<input class="form-control"  name="jam_akhir" value="{{$jam_selesai}}" readonly=""  type="text">	
									</div>		
									</div>
								</div>

								<div class="form-group form-group-last">
									<label for="exampleTextarea">Maksud / Tujuan Lembur</label>
									<textarea class="form-control" id="tujuan" name="tujuan" readonly rows="2">{{$perihal}}</textarea>
									<div class="invalid-feedback">Silahkan isi tujuan</div>
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Tempat</label>
									<textarea class="form-control" id="Tempat" name="tempat" readonly rows="1">{{$tempat}}</textarea>
									<div class="invalid-feedback">Silahkan isi tempat</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Pemberi Tugas</label>
									-
								</div>
							</div>
						</form>

						<div class="kt-portlet__body">
						<table class="table table-striped- table-hover" id="table_id" width="100%">
							<thead>
								<tr>
									<th title="Field #1">NIP</th>
									<th title="Field #2">Nama Pegawai</th>
									<th title="Field #3">Jabatan</th>
									<th title="Field #4">Sebagai</th>
									<th title="Field #5">Mulai</th>
									<th title="Field #5">Selesai</th>
									<th title="Field #5">Lama</th>
								</tr>
							</thead>
							<tbody>
								@if($data)
								@foreach($data['data'] as $item)
								<tr>
									<td>{{$item['nip']}}</td>
									<td>{{$item['nama']}}</td>
									<td>{{$item['jabatan']}}</td>
									<td>{{$item['jenis']}}</td>
									<td>{{$item['jam_mulai']}}</td>
									<td>{{$item['jam_selesai']}}</td>
									<td>{{$item['durasi']['hours']}}</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
						</div>
						<!--end::Form-->
						<form class="kt-form" id="form_kontigensi">
						<div class="kt-portlet__body">
							<input type="hidden" name="id" id="id" value="{{$id}}">
						<div class="form-group form-group-last">
									<label for="exampleTextarea">Rincian Kerja</label>
									<textarea class="form-control" id="rincian" name="rincian" rows="2"></textarea>
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Hasil Kerja</label>
									<textarea class="form-control" id="hasil" name="hasil" rows="1"></textarea>
								</div>
						
						<div class="form-group form-group-last" style="text-align: right;">
						<br/>	
						<button onclick="insert_hasil_rincian()" type="button" class="btn btn-label-success btn-sm btn-upper">Simpan</button>
						
						</div>		
							
								
						</div>
						
							
						</form>		
					</div>

				</div>

			</div>
		</div>		
	</div>
</div>
@include('lembur.action')
@endsection