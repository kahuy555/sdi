<script type="text/javascript">
	

	function insert_next() {
		/*
		if($('#tgl').val()===''){
			//endLoadingPage();
			$( "#tgl" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tgl").removeClass( "is-invalid" );

		}
		if($('#jam_awal').val()===''){
			//endLoadingPage();
			$( "#jam_awal" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_awal").removeClass( "is-invalid" );

		}
		if($('#jam_akhir').val()===''){
			//endLoadingPage();
			$( "#jam_akhir" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_akhir").removeClass( "is-invalid" );

		}
		
		if($('#user_approval').val()===''){
			//endLoadingPage();
			$( "#user_approval" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#user_approval").removeClass( "is-invalid" );

		}	
		
		if($('#tujuan').val()===''){
			//endLoadingPage();
			$( "#tujuan" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tujuan").removeClass( "is-invalid" );

		}

		if($('#tempat').val()===''){
			//endLoadingPage();
			$( "#tempat" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tempat").removeClass( "is-invalid" );

		}

		*/
		$('#user_approval').attr("disabled", false);
 		$('#tgl').attr("disabled", false); 
 		$('#jam_awal').attr("disabled", false);
 		$('#jam_akhir').attr("disabled", false);

		 var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_lembur') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				//console.log(response[0]['id']);

				if(response['rc']==9999){

					swal.fire("error",response['msg'],"error");

				}else{

					if(response['statusCode']==200){
						console.log(response['data']);

						  swal.fire({
					           title: "Info",
					           text: "Lembur sudah disimpan ",
					           type: "success",
					           confirmButtonText: "Tutup",
					           closeOnConfirm: true
					        }).then(function(result){
					            if (result.value) {

					            	window.location.href = '{{ route('next_lembur') }}';
					                
					                /*
					                window.location.href = base_url + '/form_lembur?id='+response['data'][0]['id']+'&nomor='+response['data'][0]['nomor']+'&jam_mulai='+response['data'][0]['jam_mulai']+'&jam_selesai='+response['data'][0]['jam_selesai']+'&perihal='+response['data'][0]['perihal']+'&tanggal='+response['data'][0]['tanggal']+'&tempat='+response['data'][0]['tempat'];
					                */
					            }
					        });
						

					}else{

						swal.fire("error",response['message'],"error");	 

						$('#user_approval').attr("disabled", true);
				 		$('#tgl').attr("disabled", true); 
				 		$('#jam_awal').attr("disabled", true);
				 		$('#jam_akhir').attr("disabled", true);
				 

					}

					
						

				}

				
		        
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}


	function filter(){
		$.ajax({
			type: 'GET',
			url: base_url + '/lembur?bln='+$('#bulan').val()+'&thn='+$('#tahun').val(),
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/lembur?bln='+$('#bulan').val()+'&thn='+$('#tahun').val();
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}
	function pengawas(id,nomor,tanggal,jam_mulai,jam_selesai,perihal,tempat){
		$.ajax({
			type: 'GET',
			url: base_url + '/form_pengawas?id='+id+'&nomor='+nomor+'&tanggal='+tanggal+'&jam_mulai='+jam_mulai+'&jam_selesai='+jam_selesai+'&perihal='+perihal+'&tempat='+tempat,
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/form_pengawas?id='+id+'&nomor='+nomor+'&tanggal='+tanggal+'&jam_mulai='+jam_mulai+'&jam_selesai='+jam_selesai+'&perihal='+perihal+'&tempat='+tempat;
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}
	function petugas(id,nomor,tanggal,jam_mulai,jam_selesai,perihal,tempat){
		$.ajax({
			type: 'GET',
			url: base_url + '/form_petugas?id='+id+'&nomor='+nomor+'&tanggal='+tanggal+'&jam_mulai='+jam_mulai+'&jam_selesai='+jam_selesai+'&perihal='+perihal+'&tempat='+tempat,
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/form_petugas?id='+id+'&nomor='+nomor+'&tanggal='+tanggal+'&jam_mulai='+jam_mulai+'&jam_selesai='+jam_selesai+'&perihal='+perihal+'&tempat='+tempat;
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	/*
	function insert_pengawas() {
		if($('#jam_awal').val()===''){
			//endLoadingPage();
			$( "#jam_awal" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_awal").removeClass( "is-invalid" );

		}
		if($('#jam_akhir').val()===''){
			//endLoadingPage();
			$( "#jam_akhir" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_akhir").removeClass( "is-invalid" );

		}

	 	var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_pengawas') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				console.log(response);

				if(response['rc']==9999){

					swal.fire("error",response['msg'],"error");

				}else{

					if(response['statusCode']=='500'){
						swal.fire("error",response['msg'],"error");
					}else{

						   swal.fire({
				           title: "Info",
				           text: "Pengawas sudah disimpan ",
				           type: "success",
				           confirmButtonText: "Tutup",
				           closeOnConfirm: true
				        }).then(function(result){
				            if (result.value) {

				                window.location.href = base_url + '/form_lembur?id='+response['id']+'&nomor='+response['nomor']+'&jam_mulai='+response['jam_mulai']+'&jam_selesai='+response['jam_selesai']+'&perihal='+response['perihal']+'&tanggal='+response['tanggal']+'&tempat='+response['tempat'];
				            }
				        });

					}

					

				}

				
		        
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	*/

	function batal() {
	  window.history.back();
	}

	/*

	function insert_petugas() {
		if($('#jam_awal').val()===''){
			//endLoadingPage();
			$( "#jam_awal" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_awal").removeClass( "is-invalid" );

		}
		if($('#jam_akhir').val()===''){
			//endLoadingPage();
			$( "#jam_akhir" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_akhir").removeClass( "is-invalid" );

		}

	 	var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_petugas') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				console.log(response);

				if(response['rc']==9999){

					swal.fire("error",response['msg'],"error");

				}else{

					if(response['statusCode']==500){
						swal.fire("error",response['msg'],"error");	
					}else{

						   swal.fire({
				           title: "Info",
				           text: "Petugas sudah disimpan ",
				           type: "success",
				           confirmButtonText: "Tutup",
				           closeOnConfirm: true
				        }).then(function(result){
				            if (result.value) {

				                window.location.href = base_url + '/form_lembur?id='+response['id']+'&nomor='+response['nomor']+'&jam_mulai='+response['jam_mulai']+'&jam_selesai='+response['jam_selesai']+'&perihal='+response['perihal']+'&tanggal='+response['tanggal']+'&tempat='+response['tempat'];
				            }
				        });

					}


				}
				
		        
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	*/


	function kwitansi(id){
	
		window.location.href = base_url + '/kwintansilembur?id='+id;
	}

	function upload_kwintansi(id){
	
		if($('#file').val()===''){
			//endLoadingPage();
			$( "#file" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#file").removeClass( "is-invalid" );

		}


		var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_kwitansi_lembur') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);
				console.log(response['nomor']);
				$("#loading").css('display', 'none');
				if(response['statusCode']==500){

					swal.fire("error",response['message'],"error");


				}else{

						swal.fire({
			           title: "Info",
			           text: "Upload kwintansi berhasil ",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {
			                window.location.href = '{{ route('lembur') }}';
			            }
			        });	
				}

				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });

	}

		function insert_hasil_rincian() {

		if($('#rincian').val()===''){
			//endLoadingPage();
			$( "#rincian" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#rincian").removeClass( "is-invalid" );

		}

		if($('#hasil').val()===''){
			//endLoadingPage();
			$( "#hasil" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#hasil").removeClass( "is-invalid" );

		}

		 var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_rincian_hasil') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				//console.log(response[0]['id']);

				if(response['rc']==9999){

					swal.fire("error",response['msg'],"error");

				}else{

					if(response['statusCode']==500){

						swal.fire("error",response['msg'],"error");

					}else{

						   window.location.href = base_url + '/lembur';

					}

					
						

				}

				
		        
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	 function step_next(){

		if($('#tgl').val()===''){
			//endLoadingPage();
			$( "#tgl" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tgl").removeClass( "is-invalid" );

		}
		if($('#jam_awal').val()===''){
			//endLoadingPage();
			$( "#jam_awal" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_awal").removeClass( "is-invalid" );

		}
		if($('#jam_akhir').val()===''){
			//endLoadingPage();
			$( "#jam_akhir" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#jam_akhir").removeClass( "is-invalid" );

		}
		
		if($('#user_approval').val()===''){
			//endLoadingPage();
			$( "#user_approval" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#user_approval").removeClass( "is-invalid" );

		}	
		
		if($('#tujuan').val()===''){
			//endLoadingPage();
			$( "#tujuan" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tujuan").removeClass( "is-invalid" );

		}

		if($('#tempat').val()===''){
			//endLoadingPage();
			$( "#tempat" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tempat").removeClass( "is-invalid" );

		}


 		$( "#next_msg" ).addClass( "is-invalid" );

 		//document.getElementById('province').readOnly = true;
 		document.getElementById('tujuan').readOnly = true;
 		document.getElementById('tempat').readOnly = true;

 		$('#user_approval').attr("disabled", true);

 		$('#tgl').attr("disabled", true); 
 		$('#jam_awal').attr("disabled", true);
 		$('#jam_akhir').attr("disabled", true);

 		$('#before').hide();
 		$('#next').show();

	}
 function step_before(){

 		document.getElementById('tujuan').readOnly = false;
 		document.getElementById('tempat').readOnly = false;
 		//document.getElementById('tujuan_').readOnly = false;

 		$('#user_approval').attr("disabled", false);
 		$('#tgl').attr("disabled", false); 
 		$('#jam_awal').attr("disabled", false);
 		$('#jam_akhir').attr("disabled", false);


 		$("#next_msg").removeClass( "is-invalid" );
 		$('#before').show();
 		$('#next').hide();

	}

function insert_hasil_kerja(){

	if($('#rincian').val()===''){
			//endLoadingPage();
		$( "#rincian" ).addClass( "is-invalid" );
		return false;
	}else{
		$("#rincian").removeClass( "is-invalid" );

	}

	if($('#hasil').val()===''){
		
		$( "#hasil" ).addClass( "is-invalid" );
		return false;
	}else{
		$("#hasil").removeClass( "is-invalid" );

	}

	var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_rincian_hasil') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				//console.log(response[0]['id']);

				if(response['rc']==9999){

					swal.fire("error",response['msg'],"error");

				}else{

					if(response['statusCode']==200){
						console.log(response['data']);

						window.location.href = '{{ route('next_lembur') }}';

					}else{

						swal.fire("error",response['message'],"error");	 
				 

					}

					
						

				}

				
		        
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });

}	

function add_petugas(){

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/list_pegawai',
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.nrp+"'>"+v.nmpegawai+"</option>";
	            });

	            $('#id_petugas').html(_items);
	        }
	    });

	$("#kt_modal_petugas").modal('show');
}

function batal_add_petugas(){

	$("#kt_modal_petugas").modal('hide');
}	

function add_pengawas(){

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/list_pegawai',
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.nrp+"'>"+v.nmpegawai+"</option>";
	            });

	            $('#id_pengawas').html(_items);
	        }
	    });

	$("#kt_modal_pengawas").modal('show');
}

function batal_add_pengawas(){

	$("#kt_modal_pengawas").modal('hide');
}

function insert_petugas() {

	if($('#id_petugas').val()===''){
			//endLoadingPage();
		$( "#id_petugas" ).addClass( "is-invalid" );
			return false;
	}else{
		$("#id_petugas").removeClass( "is-invalid" );

	}

		$("#kt_modal_petugas").modal('hide');

		 var formData = document.getElementById("form_petugas");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_petugas') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);

				$("#loading").css('display', 'none');
				//window.location.href = '{{ route('add_next') }}';
				if(response['statusCode']==200){

					swal.fire({
			           title: "Info",
			           text: "Petugas Berhasil Ditambahkan",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {

			           		window.location.href = '{{ route('next_lembur') }}';
			           	}
			        });

					
						
				}else{

					swal.fire("error",response['message'],"error");
					
				}
				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

function insert_pengawas() {

	if($('#id_pengawas').val()===''){
			//endLoadingPage();
		$( "#id_pengawas" ).addClass( "is-invalid" );
			return false;
	}else{
		$("#id_pengawas").removeClass( "is-invalid" );

	}

		$("#kt_modal_pengawas").modal('hide');

		 var formData = document.getElementById("form_pengawas");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_pengawas') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);

				$("#loading").css('display', 'none');
				//window.location.href = '{{ route('add_next') }}';
				if(response['statusCode']==200){

					swal.fire({
			           title: "Info",
			           text: "Pengawas Berhasil Ditambahkan",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {

			           		window.location.href = '{{ route('next_lembur') }}';
			           	}
			        });

					
						
				}else{

					swal.fire("error",response['message'],"error");
					
				}
				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}
	function hapus_peserta(id) {
	
		$.ajax({
			type: 'GET',
			url: base_url + '/hapus_peserta?id=' + id,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);

				$("#loading").css('display', 'none');
				//window.location.href = '{{ route('add_next') }}';
				if(response['statusCode']==200){

					swal.fire({
			           title: "Info",
			           text: "Peserta Lembur Berhasil Dihapus",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {

			           		window.location.href = '{{ route('next_lembur') }}';
			           	}
			        });

					
						
				}else{

					swal.fire("error",response['message'],"error");
					
				}
				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}
	function detail(id){
		window.location.href = base_url + '/next_lembur?id=' + id ;
	}
</script>