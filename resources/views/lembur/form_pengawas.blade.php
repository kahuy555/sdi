@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Tambah Pengawas
								</h3>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div class="kt-portlet__body">
								<input type="hidden" name="id" value="{{$id}}">
								<input type="hidden" name="nomor" value="{{$nomor}}">
								<input type="hidden" name="tanggal" value="{{$tanggal}}">
								<input type="hidden" name="perihal" value="{{$perihal}}">
								<input type="hidden" name="tempat" value="{{$tempat}}">
								<input type="hidden" name="jam_mulai" value="{{$jam_mulai}}">
								<input type="hidden" name="jam_selesai" value="{{$jam_selesai}}">

								<div class="form-group">
									<label for="exampleSelect1">Unit Kerja</label>
									<select class="form-control" id="idunit" name="idunit">
										<option value="">Silahkan Pilih</option>
										@if($data['data'])
										@foreach($data['data']['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nama']}}</option>
										@endforeach
										@endif
									</select>
								</div>

								<div class="form-group">
									<label for="exampleSelect1">Pegawai</label>
									<select class="form-control" id="idpegawai" name="idpegawai">
										
									</select>
								</div>

								<div class="form-group">
									<label for="exampleSelect1">Perkiraan Jam</label>
									<div class="row">
									<div class="col-6">
										<input class="form-control init-time" id="jam_awal" name="jam_awal" readonly="" placeholder="Select time" type="text">	
									</div>
									<div class="col-6">
										<input class="form-control init-time" id="jam_akhir" name="jam_akhir" readonly="" placeholder="Select time" type="text">	
									</div>	
									<div class="invalid-feedback">Silahkan isi jam</div>		
									</div>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button onclick="insert_pengawas()" class="btn btn-primary">Submit</button>
									<button onclick="batal()" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('lembur.action')
@endsection
@section('script')
<script type="text/javascript">
		$('#idunit').on('change', function (v) {

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/pegawai/'+this.value,
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.nrp+"'>"+v.nmpegawai+"</option>";
	            });

	            $('#idpegawai').html(_items);
	        }
	    });


	});

	var KTBootstrapTimepicker = function () {
    return {
        init: function() {
            $('.init-time').timepicker({
	            minuteStep: 1,
	            defaultTime: '',
	            showSeconds: false,
	            showMeridian: false,
	            snapToStep: true
	        });
        }
    };
}();
	
</script>
@stop