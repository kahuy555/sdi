<script type="text/javascript">
	var datatables = $('.kt-datatable1');
	if (datatables) {
		datatables.each(function() {
			$(this).KTDatatable('redraw');
		});
	}



	function insert() {

		if($('#alasan').val()===''){
			//endLoadingPage();
			$( "#alasan" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#alasan").removeClass( "is-invalid" );

		}
		if($('#start').val()===''){
			//endLoadingPage();
			$( "#start" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#start").removeClass( "is-invalid" );

		}

		 var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_cuti') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				var response=JSON.parse(response);
				
				$("#loading").css('display', 'none');	
				if(response['statusCode']==200){
					   swal.fire({
			           title: "Info",
			           text: "Cuti sudah disimpan ",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {
			                window.location.href = '{{ route('cuti') }}';
			            }
			        });
				}else{
					swal.fire("info",response['message'],"info");		
				}
				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}
	function filter(){
		$.ajax({
			type: 'GET',
			url: base_url + '/cuti?bln='+$('#bulan').val()+'&thn='+$('#tahun').val(),
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/cuti?bln='+$('#bulan').val()+'&thn='+$('#tahun').val();
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	function hapus(id){
	 swal.fire({
           title: "Info",
           text: "Hapus Data Cuti",
           type: "info",
           showCancelButton: true,
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {

            	$.ajax({
					type: 'GET',
					url: base_url + '/hapus_cuti?id='+id,
					beforeSend: function () {
						$("#alertInfo").addClass('d-none');
						$("#alertError").addClass('d-none');
						$("#alertSuccess").addClass('d-none');
						$("#loading").css('display', 'block');
					},

					success: function (response) {
						console.log(response);
						//var response=JSON.parse(response);
						$("#loading").css('display', 'none');
						swal.fire({
				           title: "Info",
				           text: "Cuti berhasil dihapus ",
				           type: "success",
				           confirmButtonText: "Tutup",
				           closeOnConfirm: true
				        }).then(function(result){
				            if (result.value) {
				                window.location.href = '{{ route('cuti') }}';
				            }
				        });
					}

				}).done(function (msg) {
					$("#loading").css('display', 'none');
				}).fail(function (msg) {
					$("#loading").css('display', 'none');
					swal.fire("error",'Terjadi Kesalahan',"error");
		            // toastr.error("Terjadi Kesalahan");
		        });

            }
        });
	}
</script>