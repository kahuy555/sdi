<html>
<head>
  <title>SURAT PERINTAH PERJALANAN DINAS</title>
</head>
<body>
  <style type="text/css">
    table tr td,
    table tr th{
      font-size: 9pt;
    }
  </style>
 @php
  $peserta = \DB::select("select p.nmpegawai,j.nama from peserta_perdin pp
  left join pegawai p on p.nrp=pp.nrp
  left join jabatan j on j.id=p.idjab
  where perdin_id=".$module[0]->id."
  order by pp.id asc");
   @endphp
  <table width="100%">
    <tr>
      <td width="15" align="center"><img src="{{ public_path('img/logo.png')}}" style="width: 80px;height: 80px;"></td>
      <td>
          <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>SURAT PERINTAH PERJALANAN DINAS</u></h3>
          <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO : {{$module[0]->nomor}}</h4> 
      </td>
      
      
    </tr>
  </table>
  <table width="100%">
    <tr>
      <td>1.</td>
      <td>Penjabat yang berwenang memberikan Tugas</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->nmpegawai}} - {{$module[0]->nama}}</td>
    </tr>
    <tr>
      <td valign="top">2.</td>
      <td valign="top">Nama dan Jabatan pegawai yang diperintah</td>
      <td valign="top">:</td>
      <td style="border-bottom: 1px solid black;border-right: 1px solid black;">
      @foreach($peserta as $item)
        - {{$item->nmpegawai}} <br>
        &nbsp;&nbsp; JABATAN : {{$item->nama}} <br>
      @endforeach
      </td>
    </tr>
    <tr>
      <td>3.</td>
      <td>Tempat Berangkat</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->asal}}</td>
    </tr>
    <tr>
      <td></td>
      <td>Tempat Tujuan</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->tujuan}}</td>
    </tr>
    <tr>
      <td>4.</td>
      <td>Wilayah</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->wilayah_id}}</td>
    </tr>
    <tr>
      <td></td>
      <td>Provinsi</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->wilayah}}</td>
    </tr>
     <tr>
      <td>5.</td>
      <td>Maksud Perjalanan Dinas</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->uraian}}</td>
    </tr>
     <tr>
      <td>6.</td>
      <td>Lama Perjalanan Dinas</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->durasi}} Hari</td>
    </tr>
    <tr>
      <td></td>
      <td>Tanggal Berangkat</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{date('d M Y',strtotime($module[0]->tgl_mulai))}}</td>
    </tr>
    <tr>
      <td></td>
      <td>Tanggal Kembali</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{date('d M Y',strtotime($module[0]->tgl_selesai))}}</td>
    </tr>
    <tr>
      <td>7.</td>
      <td>Alat Angkutan yang dipergunakan</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td>8.</td>
      <td>Keterangan lain-lain</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br><br><br><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">bank bjb syariah</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">{{$module[0]->nmpegawai}}</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">( {{$module[0]->nama}} )</td>
    </tr>
    

  </table>
 
</body>
</html>