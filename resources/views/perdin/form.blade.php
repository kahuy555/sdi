@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Perdin
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-wrapper">
								<button type="button" onclick="pegawai('{{$id}}','{{$nomor}}','{{$jns_perdin}}','{{$origin}}','{{$destination}}','{{$tgl_mulai}}','{{$tgl_selesai}}');" class="btn btn-label-info btn-sm btn-upper">Tambah Pegawai</button>
								&nbsp;
								<button type="button" onclick="loadNewPage('{{ route('perdin') }}')" class="btn btn-label-danger btn-sm btn-upper">Tutup</button>
							</div>
						</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div class="kt-portlet__body">
								<div class="form-group">
									<label for="exampleSelect1">Mulai Tanggal</label>
									<div class="input-daterange input-group" id="kt_datepicker_5">
													<input type="text" class="form-control" readonly value="{{date('d M Y',strtotime($tgl_mulai))}}" id="start" name="start">
													<div class="input-group-append">
														<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
													</div>
													<input type="text" class="form-control" readonly value="{{date('d M Y',strtotime($tgl_selesai))}}" id="end" name="end">
												</div>
									<div class="invalid-feedback">Silahkan pilih tanggal</div>			
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Lama</label>
									<input type="text" name="" readonly class="form-control" value="{{$data1['durasi']}}">
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Uraian</label>
									<input type="text" name="" readonly class="form-control" value="{{$data1['uraian']}}">
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Jenis Perdin</label>
									<input type="text" name="" readonly class="form-control" value="{{$jns_perdin}}">
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Pemberi Tugas</label>
									<input type="text" name="" readonly class="form-control" value="{{$data1['approval']['nama']}}">
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Wilayah</label>
									<input type="text" name="" readonly class="form-control" value="{{$data1['wilayah']}}">
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Provinsi</label>
									<input type="text" name="" readonly class="form-control" value="{{$data1['provinsi']}}">
								</div>
								<!--
								<div class="form-group">
									<label for="exampleSelect1">Wilayah</label>
									-
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Province</label>
									-
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Tujuan</label>
									-
								</div>
								-->
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Berangkat Dari</label>
									<textarea class="form-control" readonly id="tujuan" name="origin" rows="2">{{$data1['asal']}}</textarea>
									<div class="invalid-feedback">Silahkan isi tujuan</div>
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Nama Tempat Tujuan</label>
									<textarea class="form-control" readonly id="Tempat" name="destination" rows="1">{{$data1['tujuan']}}</textarea>
									<div class="invalid-feedback">Silahkan isi tempat</div>
								</div>
								<!--
								<div class="form-group">
									<label for="exampleSelect1">Pemberi Tugas</label>
									-
								</div>
							-->
							</div>
						</form>

						<div class="kt-portlet__body">
						<table class="table table-striped- table-hover" id="table_id" width="100%">
							<thead>
								<tr>
									<th title="Field #1">NIP</th>
									<th title="Field #2">Nama Pegawai</th>
									<th title="Field #3">Jabatan</th>
								</tr>
							</thead>
							<tbody>
								@if($data)
								@foreach($data['data'] as $item)
								<tr>
									<td>{{$item['nip']}}</td>
									<td>{{$item['nmpegawai']}}</td>
									<td>{{$item['jabatan']}}</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
						</div>
						<!--end::Form-->
					</div>

				</div>

			</div>
		</div>		
	</div>
</div>
@include('perdin.action')
@endsection