
<html>
<head>
  <title>PERINCIAN ONGKOS PERJALANAN DINAS</title>
</head>
<body>
   @php
  $peserta = \DB::select("select p.nmpegawai,j.nama from peserta_perdin pp
  left join pegawai p on p.nrp=pp.nrp
  left join jabatan j on j.id=p.idjab
  where perdin_id=".$module[0]->id."
  order by pp.id asc");
   @endphp
  <table width="100%">
    <tr>
      <td width="15" align="center"><img src="{{ public_path('img/logo.png')}}" style="width: 80px;height: 80px;"></td>
      <td>
          <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>PERINCIAN ONGKOS PERJALANAN DINAS</u></h3>
          <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO : {{$module[0]->nomor}}</h4> 
      </td>
      
      
    </tr>
  </table>
  <table width="100%">
    <tr>
      <td>1.</td>
      <td>Dari</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->asal}}</td>
    </tr>
    <tr>
      <td valign="top">2.</td>
      <td valign="top">Ke</td>
      <td valign="top">:</td>
      <td style="border-bottom: 1px solid black;border-right: 1px solid black;">
        {{$module[0]->tujuan}}
      </td>
    </tr>
    <tr>
      <td>3.</td>
      <td>Wilayah</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->wilayah_id}}</td>
    </tr>
    <tr>
      <td>4.</td>
      <td>Provinsi</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{$module[0]->wilayah}}</td>
    </tr>
    <tr>
      <td>5.</td>
      <td>Kendaraan Dinas No.Pol</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td>6.</td>
      <td>Nama Pengemudi</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
     <tr>
      <td>7.</td>
      <td>Tanggal & Jam Berangkat</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{date('d M Y',strtotime($module[0]->tgl_mulai))}} & Jam :</td>
    </tr>
     <tr>
      <td>8.</td>
      <td>Tanggal & Jam Kembali</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;">{{date('d M Y',strtotime($module[0]->tgl_selesai))}} & Jam :</td>
    </tr>
    <tr>
      <td valign="top">9.</td>
      <td valign="top">Nama (Para) Petugas yang Berdinas</td>
      <td valign="top">:</td>
      <td style="border-bottom: 1px solid black;border-right: 1px solid black; text-align: justify;">
        @php
        $no=0;
        @endphp
        @foreach($peserta as $item)
        @php
        $no++;
        @endphp
        {{$no}}.{{$item->nmpegawai}} ( {{$item->nama}} ) <br>
        @endforeach
      
      </td>
    </tr>
    <tr>
      <td>10.</td>
      <td>Ongkos Angkutan Berangkat Pulang</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td>11.</td>
      <td>Ongkos Angkutan Setempat</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td>12.</td>
      <td>Ongkos Hotel/Penginapan</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td>13.</td>
      <td>Ongkos Hotel/Penginapan Driver</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td>14.</td>
      <td>Keterangan Lain-lain</td>
      <td>:</td>
      <td style="border-bottom: 1px solid black;
  border-right: 1px solid black;"></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br><br><br><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">bank <b>bjb</b> syariah</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">{{$module[0]->nmpegawai}}</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">( {{$module[0]->nama}} )</td>
    </tr>
    

  </table>

 
</body>
</html>


