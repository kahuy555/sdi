<script type="text/javascript">
	var datatables = $('.kt-datatable1');
	if (datatables) {
		datatables.each(function() {
			$(this).KTDatatable('redraw');
		});
	}



	function insert() {

		
		

		if($('#start').val()===''){
			//endLoadingPage();
			$( "#start" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#start").removeClass( "is-invalid" );

		}
		if($('#end').val()===''){
			//endLoadingPage();
			$( "#end" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#end").removeClass( "is-invalid" );

		}
		
		if($('#uraian').val()===''){
			//endLoadingPage();
			$( "#uraian" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#uraian").removeClass( "is-invalid" );

		}

		if($('#user_approval').val()===''){
			//endLoadingPage();
			$( "#user_approval" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#user_approval").removeClass( "is-invalid" );

		}

		


		if($('#wilayah').val()===''){
			//endLoadingPage();
			$( "#wilayah" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#wilayah").removeClass( "is-invalid" );

		}

		

	

		 var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_perdin') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);
				console.log(response['nomor']);

				$("#loading").css('display', 'none');
				window.location.href = '{{ route('add_next') }}';
				/*
				swal.fire({
		           title: "Info",
		           text: "Perdin sudah disimpan ",
		           type: "success",
		           confirmButtonText: "Tutup",
		           closeOnConfirm: true
		        }).then(function(result){
		            if (result.value) {

		            	
		                window.location.href = base_url + '/form_perdin?id='+response[0]['id']+'&nomor='+response[0]['nomor']+'&tgl_mulai='+response[0]['tgl_mulai']+'&tgl_selesai='+response[0]['tgl_selesai']+'&jns_perdin='+response[0]['jns_perdin']+'&user='+response[0]['user_id_svp']+'&wilayah='+response[0]['wilayah_id']+'&destination='+response[0]['destination']+'&origin='+response[0]['origin'];
		                
		            }
		        });
		        */
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

		function filter(){
		$.ajax({
			type: 'GET',
			url: base_url + '/perdin?bln='+$('#bulan').val()+'&thn='+$('#tahun').val(),
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/perdin?bln='+$('#bulan').val()+'&thn='+$('#tahun').val();
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	function insert_pegawai() {
	 	var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_pegawai') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				console.log(response);
				swal.fire({
		           title: "Info",
		           text: "Pegawai sudah disimpan ",
		           type: "success",
		           confirmButtonText: "Tutup",
		           closeOnConfirm: true
		        }).then(function(result){
		            if (result.value) {

		                window.location.href =   window.location.href = base_url + '/form_perdin?id='+response['id']+'&nomor='+response['nomor']+'&tgl_mulai='+response['tgl_mulai']+'&tgl_selesai='+response['tgl_selesai']+'&jns_perdin='+response['jns_perdin']+'&destination='+response['destination']+'&origin='+response['origin'];
		            }
		        });
		        
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	function batal() {
	  window.history.back();
	}


	function pegawai(id,nomor,jns_perdin,origin,destination,tgl_mulai,tgl_selesai){
		$.ajax({
			type: 'GET',
			url: base_url + '/form_pegawai?id='+id+'&nomor='+nomor+'&jns_perdin='+jns_perdin+'&tgl_mulai='+tgl_mulai+'&tgl_selesai='+tgl_selesai+'&origin='+origin+'&destination='+destination,
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/form_pegawai?id='+id+'&nomor='+nomor+'&jns_perdin='+jns_perdin+'&tgl_mulai='+tgl_mulai+'&tgl_selesai='+tgl_selesai+'&origin='+origin+'&destination='+destination;
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	function hapus(id){
	 swal.fire({
           title: "Info",
           text: "Hapus Data Perdin",
           type: "info",
           showCancelButton: true,
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {

            	$.ajax({
					type: 'GET',
					url: base_url + '/hapus_perdin?id='+id,
					beforeSend: function () {
						$("#alertInfo").addClass('d-none');
						$("#alertError").addClass('d-none');
						$("#alertSuccess").addClass('d-none');
						$("#loading").css('display', 'block');
					},

					success: function (response) {
						console.log(response);
						//var response=JSON.parse(response);
						$("#loading").css('display', 'none');
						swal.fire({
				           title: "Info",
				           text: "Perdin berhasil dihapus ",
				           type: "success",
				           confirmButtonText: "Tutup",
				           closeOnConfirm: true
				        }).then(function(result){
				            if (result.value) {
				                window.location.href = '{{ route('perdin') }}';
				            }
				        });
					}

				}).done(function (msg) {
					$("#loading").css('display', 'none');
				}).fail(function (msg) {
					$("#loading").css('display', 'none');
					swal.fire("error",'Terjadi Kesalahan',"error");
		            // toastr.error("Terjadi Kesalahan");
		        });

            }
        });
	}
	function kwitansi(id){
	
		window.location.href = base_url + '/kwintansi?id='+id;
	}

	function upload_kwintansi(id){
	
		if($('#file').val()===''){
			//endLoadingPage();
			$( "#file" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#file").removeClass( "is-invalid" );

		}


		var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_kwitansi') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);
				console.log(response['nomor']);
				$("#loading").css('display', 'none');
				if(response['statusCode']==500){

					swal.fire("error",response['message'],"error");


				}else{

						swal.fire({
			           title: "Info",
			           text: "Upload kwintansi berhasil ",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {
			                window.location.href = '{{ route('perdin') }}';
			            }
			        });	
				}

				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });

	}


	function insert_next() {

		$('#province').attr("disabled", false); 
 		$('#tujuan_').attr("disabled", false);	

		 var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_perdin_new') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);

				$("#loading").css('display', 'none');
				//window.location.href = '{{ route('add_next') }}';
				if(response['statusCode']==200){

					window.location.href = '{{ route('detail_perdin') }}';
						
				}else{

					swal.fire("error",response['message'],"error");
					$('#province').attr("disabled", true); 
	 				$('#tujuan_').attr("disabled", true); 

				}
				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

 function step_next(){

		if($('#province').val()===''){
			//endLoadingPage();
			$( "#province" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#province").removeClass( "is-invalid" );

		}

		if($('#tujuan').val()===''){
			//endLoadingPage();
			$( "#tujuan" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tujuan").removeClass( "is-invalid" );

		}


		if($('#tempat').val()===''){
			//endLoadingPage();
			$( "#tempat" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#tempat").removeClass( "is-invalid" );

		}


 		$( "#next_msg" ).addClass( "is-invalid" );

 		//document.getElementById('province').readOnly = true;
 		document.getElementById('tujuan').readOnly = true;
 		document.getElementById('tempat').readOnly = true;
 		//document.getElementById('tujuan_').readOnly = true;

 		$('#province').attr("disabled", true); 
 		$('#tujuan_').attr("disabled", true); 

 		$('#before').hide();
 		$('#next').show();

	}
 function step_before(){

 		document.getElementById('tujuan').readOnly = false;
 		document.getElementById('tempat').readOnly = false;

 		$( "#province" ).prop( "disabled", false );	
		//$( "#tujuan" ).prop( "disabled", false );	
		//$( "#tempat" ).prop( "disabled", false );	
		$( "#tujuan_" ).prop( "disabled", false );		

 		$("#next_msg").removeClass( "is-invalid" );
 		$('#before').show();
 		$('#next').hide();

	}

function add_pegawai(){

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/list_pegawai',
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.nrp+"'>"+v.nmpegawai+"</option>";
	            });

	            $('#id_pegawai').html(_items);
	        }
	    });

	$("#kt_modal_1").modal('show');
}

function batal_add_pegawai(){

	$("#kt_modal_1").modal('hide');
}			

function insert_pegawai() {

	if($('#id_pegawai').val()===''){
			//endLoadingPage();
		$( "#id_pegawai" ).addClass( "is-invalid" );
			return false;
	}else{
		$("#id_pegawai").removeClass( "is-invalid" );

	}

		$("#kt_modal_1").modal('hide');

		 var formData = document.getElementById("form_pegawai");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });
		$.ajax({
			type: 'POST',
			url: '{{ route('insert_pegawai') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);

				$("#loading").css('display', 'none');
				//window.location.href = '{{ route('add_next') }}';
				if(response['statusCode']==200){

					swal.fire({
			           title: "Info",
			           text: "Pegawai Berhasil Ditambahkan",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {

			           		window.location.href = '{{ route('detail_perdin') }}';
			           	}
			        });

					
						
				}else{

					swal.fire("error",response['message'],"error");
					
				}
				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}

	function hapus_pegawai(id) {
	
		$.ajax({
			type: 'GET',
			url: base_url + '/hapus_pegawai?id=' + id,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				
				var response=JSON.parse(response);
				console.log(response);

				$("#loading").css('display', 'none');
				//window.location.href = '{{ route('add_next') }}';
				if(response['statusCode']==200){

					swal.fire({
			           title: "Info",
			           text: "Pegawai Berhasil Dihapus",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {

			           		window.location.href = '{{ route('detail_perdin') }}';
			           	}
			        });

					
						
				}else{

					swal.fire("error",response['message'],"error");
					
				}
				
				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}	
	function detail(id){
		window.location.href = base_url + '/detail_perdin?id=' + id ;
	}
</script>