@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Perjalanan Dinas (Perdin)
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-wrapper">
									<button type="button" onclick="loadNewPage('{{ route('perdin') }}')"  class="btn btn-danger btn-sm btn-upper">Perdin Sebelumnya</button>
								</div>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<input type="hidden" name="start" value="{{Session('start')}}">
							<input type="hidden" name="end" value="{{Session('end')}}">
							<input type="hidden" name="uraian" value="{{Session('uraian')}}">
							<input type="hidden" name="jns" value="{{Session('jns')}}">
							<input type="hidden" name="wilayah" value="{{Session('wilayah')}}">
							<input type="hidden" name="user_approval" value="{{Session('user_approval')}}">

							<div class="kt-portlet__body">
								<div class="form-group row">
									<label for="example-email-input" class="col-2 col-form-label">NIP</label>
									<div class="col-10">
										<label for="example-email-input" class="col-form-label">: &nbsp;{{Session('nip')}}</label>
									</div>
									
									<label for="example-email-input" class="col-2 col-form-label">Dari Tanggal</label>
									<div class="col-10">
										<label for="example-email-input" class="col-form-label">: &nbsp;{{date('d M Y',strtotime(Session('start')))}} s.d {{date('d M Y',strtotime(Session('end')))}}</label>
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Uraian</label>
									<div class="col-10">
										<label for="example-email-input" class="col-form-label">: &nbsp;{{Session('uraian')}}</label>
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Jenis Perdin</label>
									<div class="col-10">
										<label for="example-email-input" class="col-form-label">: &nbsp;{{Session('jns')}}</label>

									</div>

									<label for="example-email-input" class="col-2 col-form-label">Pemberi Tugas</label>
									<div class="col-10">
										<label for="example-email-input" class="col-form-label">: &nbsp;{{Session('pemberi_tugas')}}</label>
										
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Wilayah</label>
									<div class="col-10">
										<label for="example-email-input" class="col-form-label">: &nbsp;{{Session('nama_wilayah')}}</label>
										
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Provinsi</label>
									<div class="col-10">
										<select class="form-control"  name="province" id="province">
											<option value="">Silahkan Pilih Provinsi</option>
											@foreach($provinsi as $item)
												<option value="{{$item['id']}}">{{$item['nama']}}</option>
											@endforeach
										</select>
										<div class="invalid-feedback">Silahkan Pilih Provinsi</div>
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Berangkat Dari</label>
									<div class="col-10">
										<input type="text" class="form-control" id="tujuan" name="origin">
										<div class="invalid-feedback">Silahkan isi tujuan</div>
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Tujuan</label>
									<div class="col-10">
										<select class="form-control"  name="tujuan" id="tujuan_">
											<option value="1">KP/Cabang</option>
											<option value="2">Luar Kantor</option>
										</select>
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Nama Tempat Tujuan</label>
									<div class="col-10">
										<input type="text" class="form-control" id="tempat" name="destination">
										<div class="invalid-feedback">Silahkan isi tempat</div>
									</div>

									<div class="col-10" >
										<input type="hidden" class="form-control" id="next_msg">
										<div class="invalid-feedback">Data Perdin Sudah Lengkap</div>
									</div>

								</div>
							</div>
							
							<div class="kt-portlet__foot">
								<div class="kt-form__actions" style="text-align: right;">
									<div id="next" style="display: none;">
										<button onclick="step_before()" class="btn btn-danger">Lakukan Perubahan Data</button>
										<button onclick="insert_next()" class="btn btn-info">Simpan</button>
										<button onclick="step_before()" class="btn btn-secondary">Batal</button>
									</div>
									<div id="before">
										<button onclick="step_next()" class="btn btn-primary">Lanjutkan</button>
										<button onclick="goBack()" class="btn btn-secondary">Batal</button>
									</div>
									
									
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('perdin.action')
@endsection
@section('script')

<script>
function goBack() {
  window.history.back();
}
</script>

<script type="text/javascript">
var KTBootstrapDatepicker = function () {
    return {
        init: function() {
            $('.init-date').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                constrainInput: true
               	 
            });
        }
    };
}();	

	$('#wilayah').on('change', function (v) {

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/wilayah/'+this.value,
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.id+"'>"+v.nama+"</option>";
	            });

	            $('#province').html(_items);
	        }
	    });


	});
</script>
@stop