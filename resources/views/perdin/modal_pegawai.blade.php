<div class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Pegawai</h5>
			</div>
			<div class="modal-body">
				<form id="form_pegawai">
					<div>
						<label for="example-email-input" class="col-4 col-form-label">Nama Pegawai</label>
						<div class="col-8">
							<select name="id_pegawai" id="id_pegawai" class="form-control select">
							</select>
							<div class="invalid-feedback">Silahkan Pilih Pegawai</div>
						</div>
					</div>
					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="batal_add_pegawai()">Batal</button>
				<button type="button" class="btn btn-primary" onclick="insert_pegawai()">Simpan</button>
			</div>
		</div>
	</div>
</div>