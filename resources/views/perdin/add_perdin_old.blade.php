@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Perjalanan Dinas (Perdin)
								</h3>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div class="kt-portlet__body">
								<div class="form-group">
									<label for="exampleSelect1">NIP</label>
									<label class="form-control">{{Session('nip')}}</label>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Dari Tanggal</label>
									<div class="input-daterange input-group init-date">
										<input type="text" class="form-control" id="start" name="start">
											<div class="input-group-append">
												<span class="input-group-text">s.d</span>
											</div>
										<input type="text" class="form-control" id="end" name="end">
										<div class="invalid-feedback">Silahkan Pilih Tanggal</div>
									</div>
											
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Jenis Perdin</label>
									<select class="form-control"  name="jns">
										<option value="Dinas">Dinas</option>
										<option value="Pelatihan">Pelatihan</option>
									</select>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Wilayah</label>
									<select class="form-control"  name="wilayah" id="wilayah">
										<option value="">Silahkan Pilih</option>
										<option value="1">Wilayah 1</option>
										<option value="2">Wilayah 2</option>
										<option value="3">Wilayah 3</option>
									</select>
									<div class="invalid-feedback">Silahkan Pilih Wilayah</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Provinsi</label>
									<select class="form-control"  name="province" id="province">
									</select>
									<div class="invalid-feedback">Silahkan Pilih Provinsi</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Tujuan</label>
									<select class="form-control"  name="tujuan">
										<option value="1">KP/Cabang</option>
										<option value="2">Luar Kantor</option>
									</select>
								</div>

								<div class="form-group form-group-last">
									<label for="exampleTextarea">Berangkat Dari</label>
									<textarea class="form-control" id="tujuan" name="origin" rows="2"></textarea>
									<div class="invalid-feedback">Silahkan isi tujuan</div>
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Nama Tempat Tujuan</label>
									<textarea class="form-control" id="tempat" name="destination" rows="1"></textarea>
									<div class="invalid-feedback">Silahkan isi tempat</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Pemberi Tugas</label>
									<select class="form-control" id="exampleSelect1" name="user_approval">
										@if($data['data'])
										@foreach($data['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nmpegawai']}}</option>
										@endforeach
										@endif
									</select>
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Uraian</label>
									<textarea class="form-control" id="uraian" name="uraian" rows="2"></textarea>
									<div class="invalid-feedback">Silahkan isi uraian</div>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions" style="text-align: right;">
									<button onclick="insert()" class="btn btn-primary">Lanjutkan</button>
									<button onclick="loadNewPage('{{ route('perdin') }}')" class="btn btn-secondary">Batal</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('perdin.action')
@endsection
@section('script')
<script type="text/javascript">
var KTBootstrapDatepicker = function () {
    return {
        init: function() {
            $('.init-date').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                constrainInput: true
               	 
            });
        }
    };
}();	

	$('#wilayah').on('change', function (v) {

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/wilayah/'+this.value,
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.id+"'>"+v.nama+"</option>";
	            });

	            $('#province').html(_items);
	        }
	    });


	});
</script>
@stop