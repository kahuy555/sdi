@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">

		<!--begin:: Widgets/Quick Stats-->
		<div class="row row-full-height">
			<div class="col-sm-12 col-md-12 col-lg-6">
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-brand">
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget26">
							<div class="kt-widget26__content">
								<span class="kt-widget26__number">Absensi</span>
								<span class="kt-widget26__desc">
									Perhitungan gaji akan diproses, bagi pegawai yang belum melengkapi kontingency untuk segera menyelesaikannya pada kesempatan pertama.
									<br><br>
									<p class="text-center">- ADKOM -</p>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-space-20"></div>
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-danger">
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget26">
							<div class="kt-widget26__content">
								<span class="kt-widget26__number">Pemenuhan KPI TW IV 2019</span>
								<span class="kt-widget26__desc text-danger" style="font-weight:bold;font-size:14px;font-style:italic;">17-26 Feb KPI TW IV 2019</span><br>
								Bagi pegawai yang belum mengisi KPI TW IV di tahun 2019, diharpkan segera melakukan pengisian KPI. Dikarenakan data akan segera diproses.
								<br><br>
								<p class="text-center">- RPK -</p>
							</div>
							<!-- <div class="chartdiv"></div>
							<div class="kt-widget26__chart" style="height:100px; width: 230px;">
								<canvas id="kt_chart_quick_stats_2"></canvas>
							</div> -->
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-6">
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-success">
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget26">
							<div class="kt-widget26__content">
								<span class="kt-widget26__number">Visi Misi bank bjb Syariah</span>
								<span class="kt-widget26__desc">Visi : <br> Menjadi 5 bank Syariah terbesar di Indonesia, berkinerja baik dan menjadi solusi keuangan pilihan masyarakat.</span>
								<br>
								<p class="text-center">- BUDAYA -</p>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-space-20"></div>
				<div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-warning">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Jadwal Pelatihan Bulan Ini
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<span style="border:1px solid #5578eb; padding: 8px; border-radius: 5px;"><b>FEBRUARI 2020</b></span>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="table-responsive">					 
							<table class="table">
								<tr>
									<td style="vertical-align: middle;" width="105px">05-07 Feb</td>
									<td>
										<span class="kt-widget11__title"><b>PDPS</b></span><br>
										<span class="kt-widget11__sub">Pelatihan Dasar Perbankan Syariah Berskala Internasional</span>
									</td>
									<td style="vertical-align: middle;">
										Hotel California Bandung
									</td>
									<td style="vertical-align: middle;" width="94px">
										<span class="kt-badge kt-badge--success kt-badge--inline">selesai</span>
									</td>
								</tr>
								<tr>
									<td style="vertical-align: middle;" width="103px">25-29 Feb</td>
									<td>
										<span class="kt-widget11__title"><b>APBL</b></span><br>
										<span class="kt-widget11__sub">Pengenalan Core Banking System</span>
									</td>
									<td style="vertical-align: middle;">
										Putri Gunug Hotel Lembang
									</td>
									<td style="vertical-align: middle;" width="94px">
										<span class="kt-badge kt-badge--danger kt-badge--inline">in process</span>
									</td>
								</tr>
							</table>
						</div>						
					</div>
				</div>
			</div>
		</div>

		<!--end:: Widgets/Quick Stats-->
	</div>

</div>
@endsection