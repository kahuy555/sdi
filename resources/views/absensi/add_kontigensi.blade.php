@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Kontigensi Absensi Tanggal {{date('d M Y',strtotime($tgl))}}
								</h3>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<input type="hidden" name="tgl" value="{{$tgl}}">
							<input type="hidden" name="id" value="{{$id}}">
							<div class="kt-portlet__body">
								<div class="form-group">
									<label>Jam Datang</label>
									@if($jamMasuk)
									<div class="input-group timepicker">
										<input class="form-control" value="{{date('H:i',strtotime($jamMasuk))}}" name="jam_masuk" readonly="" placeholder="Select time" type="text">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-clock-o"></i>
											</span>
										</div>
									</div>
									@else
									<div class="input-group timepicker">
										<input class="form-control" id="kt_timepicker_2" name="jam_masuk" readonly="" placeholder="Select time" type="text">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<div class="invalid-feedback">Silahkan isi jam Masuk</div>
									</div>
									@endif
									
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Jam Pulang</label>
									@if($jamKeluar)
									<div class="input-group timepicker">
										<input class="form-control" value="{{date('H:i',strtotime($jamKeluar))}}" name="jam_keluar" readonly="" placeholder="Select time" type="text">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-clock-o"></i>
											</span>
										</div>
									</div>
									@else
									<div class="input-group timepicker">
										<input class="form-control" id="kt_timepicker_2_2" name="jam_keluar" readonly="" placeholder="Select time" type="text">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<div class="invalid-feedback">Silahkan isi jam Keluar</div>
									</div>
									@endif
									
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Alasan</label>
									<textarea class="form-control" id="alasan" name="alasan" rows="3"></textarea>
									<div class="invalid-feedback">Silahkan isi alasan</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Diputuskan</label>
									<select class="form-control" id="exampleSelect1" name="user_approval">
										@if($data['data'])
										@foreach($data['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nmpegawai']}}</option>
										@endforeach
										@endif
									</select>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button onclick="insert_kontigensi()" class="btn btn-primary">Submit</button>
									<button onclick="loadNewPage('{{ route('absensi') }}')" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('absensi.action')
@endsection