<script type="text/javascript">
	

	function add_kontigensi(idabsen,tgl,jamMasuk,jamKeluar) {
		$.ajax({
			type: 'GET',
			url: base_url + '/add_kontigensi?id='+idabsen+'&tgl='+tgl+'&jamMasuk='+jamMasuk+'&jamKeluar='+jamKeluar,
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},
			success: function (response) {
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/add_kontigensi?id='+idabsen+'&tgl='+tgl+'&jamMasuk='+jamMasuk+'&jamKeluar='+jamKeluar;
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
            
        });
	}


	function insert_kontigensi() {

		if($('#alasan').val()===''){
			//endLoadingPage();
			$( "#alasan" ).addClass( "is-invalid" );
			return false;
		}else{
			$("#alasan").removeClass( "is-invalid" );

		}

		 var formData = document.getElementById("form_kontigensi");
		 var objData = new FormData(formData);

		 $.ajaxSetup({
		 	headers: {
		 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 	}
		 });

		$.ajax({
			type: 'POST',
			url: '{{ route('insert_kontigensi') }}',
			data: objData,
			contentType: false,
			cache: false,
			processData: false,

			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');

				console.log('===========');
				console.log(response['statusCode']);
				console.log(response.statusCode);
				console.log('===========');

				if(response.statusCode==403){
					swal.fire("info",response.message,"info");
				}else{
					swal.fire({
			           title: "Info",
			           text: "Kontigensi sudah disimpan ",
			           type: "success",
			           confirmButtonText: "Tutup",
			           closeOnConfirm: true
			        }).then(function(result){
			            if (result.value) {
			                window.location.href = '{{ route('absensi') }}';
			            }
			        });

				}

				
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (response) {
			 console.log(objData);
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });

	}

	function filter(){
		$.ajax({
			type: 'GET',
			url: base_url + '/absensi?bln='+$('#bulan').val()+'&thn='+$('#tahun').val(),
			beforeSend: function () {
				$("#alertInfo").addClass('d-none');
				$("#alertError").addClass('d-none');
				$("#alertSuccess").addClass('d-none');
				$("#loading").css('display', 'block');
			},

			success: function (response) {
				console.log(response);
				//var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				window.location.href = base_url + '/absensi?bln='+$('#bulan').val()+'&thn='+$('#tahun').val();
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });
	}
</script>